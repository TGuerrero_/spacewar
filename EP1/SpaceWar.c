 
 //#################################################################
/*
 Nomes:                                NUSP:
 Erick Rodrigues de Santana          11222008
 Matheus Koiti Ito 	                 9296571        
 Thiago Guerrero                     11275297

------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//##################################################################


#define MAX 1000
#define EXIT_FAILURE 1
#define EXIT_SUCCESS 0
#define MAX_TIME 0.001
#define CTE_GRAVITACIONAL 0.0000000000667

typedef struct {
    char nome[80];
    double massa;
    double posx, posy;
    double velx, vely;
    double forcax, forcay;
} nave;

typedef struct {
    double massa;
    double raio;
} planeta;

typedef struct {
    double massa;
    double posx, posy;
    double velx, vely;
    double forcax, forcay;
} projetil;


/*######################################################################
                            Variável GLobal
######################################################################*/

nave player1, player2;
planeta terra;
projetil *tiros;
int qtdProjetil;
double duracaoProjetil;

//#####################################################################

/*Funções*/

int input(double *tempoTotal);
void output();

void forcaNave1();
void forcaNave2();
void forcaProjetil();

double forcaGravitacional(double massa1, double massa2, double dist);
double distancia(double posX1, double posY1, double posX2, double posY2);
double novaPosicao ();
void novaVelocidade ();
int referencial(double pos1, double pos2);
void leituraEntradas(double tempoTotal);
void tabelaSaidas(int i, double cont, double tempoTotal);

//#####################################################################

int main() {
    double tempoTotal, cont;
    int check, i, j;

    printf ("\n             # SPACE WAR #             \n");

    check = input (&tempoTotal);

    if (check == EXIT_FAILURE){
        printf ("Ocorreu um erro na leitura do arquivo, tente novamente!");
        return EXIT_FAILURE;
    }

    leituraEntradas(tempoTotal);

    for (i=0, cont= 0.00; cont < tempoTotal; cont += MAX_TIME, i++){
        //Checa se o tempo de duração dos projéteis já estourou.
        if (cont >= duracaoProjetil){
            qtdProjetil = 0;
        }
        forcaNave1();
        forcaNave2();
        forcaProjetil();

        //Na primeira iteração, orienta os objetos na direção da força resultante.
        if (i == 0){
            if (player1.forcax < 0)
                player1.velx = -player1.velx;
            if (player1. forcay < 0)
                player1.vely = -player1.vely;

            if (player2.forcax < 0)
                player2.velx = -player2.velx;
            if (player2. forcay < 0)
                player2.vely = -player2.vely;

            for (j=0; j < qtdProjetil; j++){
                if (tiros[j].forcax < 0)
                    tiros[j].velx = -tiros[j].velx;
                if (tiros[j]. forcay < 0)
                    tiros[j].vely = -tiros[j].vely;
            }
        }
        novaVelocidade();
        novaPosicao();
        tabelaSaidas(i, cont+MAX_TIME, tempoTotal);
    }

    free(tiros);
    return 0;
}

/*Implementação - Funções*/

int input(double *tempoTotal){
    FILE * load;
    char in[80];
    int i;

    printf ("Digite o nome do arquivo a ser lido: ");
    scanf ("%s", in);
    load = fopen(in, "r");
    if (load == NULL)
        return EXIT_FAILURE;

    fscanf (load, "%lf %lf %lf", &(terra.raio), &(terra.massa), &(*tempoTotal));
    fscanf (load, "%s %lf %lf %lf %lf %lf", &(*player1.nome), &(player1.massa), &(player1.posx), &(player1.posy), &(player1.velx), &(player1.vely));
    fscanf (load, "%s %lf %lf %lf %lf %lf", &(*player2.nome), &(player2.massa), &(player2.posx), &(player2.posy), &(player2.velx), &(player2.vely));
    fscanf (load, "%d %lf", &(qtdProjetil), &(duracaoProjetil));

    tiros = malloc(qtdProjetil * sizeof (projetil));

    for (i=0; i < qtdProjetil; i++){
        fscanf (load, "%lf %lf %lf %lf %lf", &(tiros[i].massa), &(tiros[i].posx), &(tiros[i].posy), &(tiros[i].velx), &(tiros[i].vely));
    }
    fclose (load);
    return EXIT_SUCCESS;
}

/*
* ForcaNave1();
* Faz o cálculo da força resultante na Nave 1 e armazena na própria struct, 
* não recebe nenhum parâmetro pois altera variáveis globais.
*/
void forcaNave1(){
    int i, direcao;
    double dist, seno, cosseno;
    double forcaxLocal, forcayLocal, forcax=0, forcay=0, forcaGrav;

    //Calculo da força em relação ao planeta
    dist = distancia(player1.posx, player1.posy, 0, 0);
    forcaGrav = forcaGravitacional(terra.massa, player1.massa, dist);
    seno = abs(player1.posy-0)/dist; 
    cosseno = abs(player1.posx-0)/dist;

    direcao = referencial(0, player1.posx);
    forcaxLocal = direcao * forcaGrav * cosseno;
    direcao = referencial(0, player1.posy);
    forcayLocal = direcao * forcaGrav * seno;
    forcax += forcaxLocal;
    forcay += forcayLocal;

    //Calculo da força em relação aos projéteis
    for (i=0; i < qtdProjetil; i++){
        dist = distancia(player1.posx, player1.posy, tiros[i].posx, tiros[i].posy);
        forcaGrav = forcaGravitacional(player1.massa, tiros[i].massa, dist);
        seno = abs(player1.posy - tiros[i].posy)/dist;
        cosseno = abs(player1.posx - tiros[i].posx)/dist;

        direcao = referencial(tiros[i].posx, player1.posx);
        forcaxLocal = direcao * forcaGrav * cosseno;
        direcao = referencial(tiros[i].posy, player1.posy);
        forcayLocal = direcao * forcaGrav * seno;

        forcax += forcaxLocal;
        forcay += forcayLocal;
    }

    //Calculo da força em relação à nave 2
    dist = distancia(player1.posx, player1.posy, player2.posx, player2.posy);
    forcaGrav = forcaGravitacional(player1.massa, player2.massa, dist);
    seno = abs(player2.posy - player1.posy)/dist;
    cosseno = abs(player2.posx - player1.posx)/dist;

    direcao = referencial(player2.posx, player1.posx);
    forcaxLocal = direcao * forcaGrav * cosseno;
    direcao = referencial(player2.posy, player1.posy);
    forcayLocal = direcao * forcaGrav * seno;

    forcax += forcaxLocal;
    forcay += forcayLocal;

    //Armazenamento da força resultante
    player1.forcax = forcax;
    player1.forcay = forcay;
}

/*
* ForcaNave2();
* Faz o cálculo da força resultante na Nave 2 e armazena na própria struct, 
* não recebe nenhum parâmetro pois altera variáveis globais.
*/
void forcaNave2(){
    int i, direcao;
    double dist, seno, cosseno;
    double forcaxLocal, forcayLocal, forcax=0, forcay=0, forcaGrav;

    //Calculo da força em relação ao planeta
    dist = distancia(player2.posx, player2.posy, 0, 0);
    forcaGrav = forcaGravitacional(terra.massa, player2.massa, dist);
    seno = abs(player2.posy-0)/dist; 
    cosseno = abs(player2.posx-0)/dist;

    direcao = referencial(0, player2.posx);
    forcaxLocal = direcao * forcaGrav * cosseno;
    direcao = referencial(0, player2.posy);
    forcayLocal = direcao * forcaGrav * seno;
    forcax += forcaxLocal;
    forcay += forcayLocal;

    //Calculo da força em relação aos projéteis
    for (i=0; i < qtdProjetil; i++){
        dist = distancia(player2.posx, player2.posy, tiros[i].posx, tiros[i].posy);
        forcaGrav = forcaGravitacional(player2.massa, tiros[i].massa, dist);
        seno = abs(player1.posy - tiros[i].posy)/dist;
        cosseno = abs(player1.posx - tiros[i].posx)/dist;

        direcao = referencial(tiros[i].posx, player2.posx);
        forcaxLocal = direcao * forcaGrav * cosseno;
        direcao = referencial(tiros[i].posy, player2.posy);
        forcayLocal = direcao * forcaGrav * seno;

        forcax += forcaxLocal;
        forcay += forcayLocal;
    }

    //Calculo da força em relação à nave 1
    dist = distancia(player1.posx, player1.posy, player2.posx, player2.posy);
    forcaGrav = forcaGravitacional(player1.massa, player2.massa, dist);
    seno = abs(player2.posy - player1.posy)/dist;
    cosseno = abs(player2.posx - player1.posx)/dist;

    direcao = referencial(player1.posx, player2.posx);
    forcaxLocal = direcao * forcaGrav * cosseno;
    direcao = referencial(player1.posy, player2.posy);
    forcayLocal = direcao * forcaGrav * seno;

    forcax += forcaxLocal;
    forcay += forcayLocal;

    //Armazenamento da força resultante
    player2.forcax = forcax;
    player2.forcay = forcay;
}

/*
* ForcaProjetil();
* Faz o cálculo da força resultante nos "qtdProjetil" projéteis e armazena na própria struct, 
* não recebe nenhum parâmetro pois altera variáveis globais.
*/
void forcaProjetil(){
    int i, j, direcao;
    double dist, seno, cosseno;
    double forcaxLocal, forcayLocal, forcax=0, forcay=0, forcaGrav;

    for (i = 0; i < qtdProjetil; i++, forcax=0,forcay=0) {
        // Calculo da força em relação ao planeta
        dist = distancia(tiros[i].posx, tiros[i].posy, 0, 0);
        forcaGrav = forcaGravitacional(terra.massa, tiros[i].massa, dist);
        seno = abs(tiros[i].posy-0)/dist; 
        cosseno = abs(tiros[i].posx)-0/dist;

        direcao = referencial(0, tiros[i].posx);
        forcaxLocal = direcao * forcaGrav * cosseno;
        direcao = referencial(0, tiros[i].posy);
        forcayLocal = direcao * forcaGrav * seno;
        forcax += forcaxLocal;
        forcay += forcayLocal;

        // Calculo da força em relação à primeira nave
        dist = distancia(tiros[i].posx, tiros[i].posy, player1.posx, player1.posy);
        forcaGrav = forcaGravitacional(tiros[i].massa, player1.massa, dist);
        seno = abs(tiros[i].posy - player1.posy)/dist;
        cosseno = abs(tiros[i].posx - player1.posx)/dist;

        direcao = referencial(player1.posx, tiros[i].posx);
        forcaxLocal = direcao * forcaGrav * cosseno;
        direcao = referencial(player1.posy, tiros[i].posy);
        forcayLocal = direcao * forcaGrav * seno;

        forcax += forcaxLocal;
        forcay += forcayLocal;

        // Calculo da força em relação à segunda nave
        dist = distancia(tiros[i].posx, tiros[i].posy, player2.posx, player2.posy);
        forcaGrav = forcaGravitacional(tiros[i].massa, player2.massa, dist);
        seno = abs(tiros[i].posy - player2.posy)/dist;
        cosseno = abs(tiros[i].posx - player2.posx)/dist;

        direcao = referencial(player2.posx, tiros[i].posx);
        forcaxLocal = direcao * forcaGrav * cosseno;
        direcao = referencial(player2.posy, tiros[i].posy);
        forcayLocal = direcao * forcaGrav * seno;

        forcax += forcaxLocal;
        forcay += forcayLocal;

        // Calculo da força que os projeteis restantes exercem
        for (j = 0; j < qtdProjetil; j++) {
            if (j == i)
                continue;
            
            dist = distancia(tiros[i].posx, tiros[i].posy, tiros[j].posx, tiros[j].posy);
            forcaGrav = forcaGravitacional(tiros[i].massa, tiros[j].massa, dist);
            seno = abs(tiros[i].posy - tiros[j].posy)/dist;
            cosseno = abs(tiros[i].posx - tiros[j].posx)/dist;

            direcao = referencial(tiros[j].posx, tiros[i].posx);
            forcaxLocal = direcao * forcaGrav * cosseno;
            direcao = referencial(tiros[j].posy, tiros[i].posy);
            forcayLocal = direcao * forcaGrav * seno;

            forcax += forcaxLocal;
            forcay += forcayLocal;
        }
        //Armazenamento da força resultante
        tiros[i].forcax = forcax;
        tiros[i].forcay = forcay; 
    }
}

/*
* ForcaGravitacional();
* Recebe a "massa1" do primeiro corpo, a "massa2" do segundo corpo e a "dist" entre os corpos,
* retorna o módulo da força gravitacional entre os corpos.
*/
double forcaGravitacional(double massa1, double massa2, double dist){
    double forca;
    forca = (CTE_GRAVITACIONAL*massa1*massa2);
    return (forca/(dist*dist));
}

/*
* distancia();
* Recebe o par ordenado (posx, posy) de dois corpos
* e retorna a distancia entre eles.
*/
double distancia(double posX1, double posY1, double posX2, double posY2){
    double dist, distx, disty;

    distx = (posX1-posX2)*(posX1-posX2);
    disty = (posY1-posY2)*(posY1-posY2);
    dist = sqrt(distx+disty);
    return dist;
}

/*
* novaPosicao();
* Usando a força resultante em cada corpo, calcula a nova posição de todos os corpos.
* não recebe nenhum parâmetro pois altera variáveis global.
*/
double novaPosicao(){
    int i;
    double posFinal, aceleracaox, aceleracaoy;

    //Posição final da nave 1
    aceleracaox = player1.forcax/player1.massa;
    aceleracaoy = player1.forcay/player1.massa;
    
    posFinal = player1.posx + (player1.velx*MAX_TIME)+ ((aceleracaox*(MAX_TIME)*(MAX_TIME))/2);
    player1.posx = posFinal;
    posFinal = player1.posy + (player1.vely*MAX_TIME)+ ((aceleracaoy*(MAX_TIME)*(MAX_TIME))/2);
    player1.posy = posFinal;

    //Posição final da nave 2
    aceleracaox = player2.forcax/player2.massa;
    aceleracaoy = player2.forcay/player2.massa;
    
    posFinal = player2.posx + (player2.velx*MAX_TIME)+ ((aceleracaox*(MAX_TIME)*(MAX_TIME))/2);
    player2.posx = posFinal;
    posFinal = player2.posy + (player2.vely*MAX_TIME)+ ((aceleracaoy*(MAX_TIME)*(MAX_TIME))/2);
    player2.posy = posFinal;

    //Posição final dos projéteis
    for (i=0; i < qtdProjetil; i++){
        aceleracaox = tiros[i].forcax/tiros[i].massa;
        aceleracaoy = tiros[i].forcay/tiros[i].massa;

        posFinal = tiros[i].posx + (tiros[i].velx*MAX_TIME)+ ((aceleracaox*(MAX_TIME)*(MAX_TIME))/2);
        tiros[i].posx = posFinal;
        posFinal = tiros[i].posy + (tiros[i].vely*MAX_TIME)+ ((aceleracaoy*(MAX_TIME)*(MAX_TIME))/2);
        tiros[i].posy = posFinal;
    }

}

/*
* novaVelocidade();
* Usando a força resultante e as novas velocidades em cada corpo,
* calcula a nova posição de todos os corpos.
* não recebe nenhum parâmetro pois altera variáveis global.
*/
void novaVelocidade(){
    int i;
    double velFinal, aceleracaox, aceleracaoy;

    //Velocidade final da nave 1
    aceleracaox = player1.forcax/player1.massa;
    aceleracaoy = player1.forcay/player1.massa;

    velFinal = player1.velx + (aceleracaox*MAX_TIME);
    player1.velx = velFinal;
    velFinal = player1.vely + (aceleracaoy*MAX_TIME);
    player1.vely = velFinal;

    //Velocidade final da nave 2
    aceleracaox = player2.forcax/player2.massa;
    aceleracaoy = player2.forcay/player2.massa;

    velFinal = player2.velx + (aceleracaox*MAX_TIME);
    player2.velx = velFinal;
    velFinal = player2.vely + (aceleracaoy*MAX_TIME);
    player2.vely = velFinal;

    //Velocidade final dos projéteis
    for (i=0; i < qtdProjetil; i++){
        aceleracaox = tiros[i].forcax/tiros[i].massa;
        aceleracaoy = tiros[i].forcay/tiros[i].massa;

        velFinal = tiros[i].velx + (aceleracaox*MAX_TIME);
        tiros[i].velx = velFinal;
        velFinal = tiros[i].vely + (aceleracaoy*MAX_TIME);
        tiros[i].vely = velFinal;
    }
}

/*
* referencial();
* Recebe a posição x ou y do corpo 1 e do corpo 2 e retorna:
*  1 - Se o referencial for negativo;
* -1 - Se o referecial for positivo.
*
* OBS: Pos1 será sempre o corpo gerador da força;
* Pos2 será o corpo no qual a força é aplicada.
*/
int referencial(double pos1, double pos2){
    int check;
    if (pos1 - pos2 >= 0)
        check = 1;
    else
        check = -1;

    return check;
}

/*
* leituraEntradas();
* Recebe o tempo total de simulação e usando as variáveis globais,
* printa na tela os arquivos lidos na função "Input()".
*/
void leituraEntradas (double tempoTotal){
    int i;

    printf ("\n################# PRINT DAS ENTRADAS ##################\n");

    printf ("Raio do planeta: %.3lf\n", terra.raio); 
    printf ("Massa do planeta: %.3lf\n", terra.massa); 
    printf ("Tempo de duração: %.5lf\n", tempoTotal);

    printf ("\nNave 1:\n");
    printf ("Nome: %s\n", player1.nome);
    printf ("Massa: %.3lf\n", player1.massa);
    printf ("Pos x: %.6lf Pos y: %.6lf\n", player1.posx, player1.posy);
    printf ("Vel x: %.6lf Vel y: %.6lf\n", player1.velx,player1.vely);

    printf ("\nNave 2:\n");
    printf ("Nome: %s\n", player2.nome);
    printf ("Massa: %.3lf\n", player2.massa);
    printf ("Pos x: %.10lf Pos y: %.10lf\n", player2.posx, player2.posy);
    printf ("Vel x: %.10lf Vel y: %.10lf\n", player2.velx,player2.vely);

    printf ("Quantidade de projéteis: %d\n", qtdProjetil);
    printf ("Duração dos projéteis: %.5lf\n", duracaoProjetil);

    for(i=0; i < qtdProjetil; i++){ 
        printf ("\nProjétil %d:\n", i+1);
        printf ("Massa: %.3lf\n",tiros[i].massa);
        printf ("Pos x: %.10lf Pos y: %.10lf\n",tiros[i].posx, tiros[i].posy);
        printf ("Vel x: %.10lf Vel y: %.10lf\n",tiros[i].velx, tiros[i].vely);
    }
}

/*
* tabelaSaidas();
* Recebe a iteração "i" atual, o valor "cont" do contador atual e
* o tempo total de simulação. Usando as variáveis globais,
* printa na tela os dados dos corpos na iteração "i".
*/
void tabelaSaidas(int i, double cont, double tempoTotal){
    /*Função que imprime as informações durante as interações do planeta, das naves e dos projeteis.*/
    int k;

    if (i%10 == 0 && i != 0){
        printf("\nAperte qualquer tecla para prosseguir.");
        getchar();
        if (i == 10)
            getchar();
    }
    
    printf ("\n##############  %4.3lf milésimos após o início ###############\n",cont);
    //nova posicao da nave 1
    printf ("Nave 1:\n");
    printf ("Nome: %s\n", player1.nome);
    printf ("Pos x: %.10lf Pos y: %.10lf\n", player1.posx, player1.posy);
    printf ("Vel x: %.10lf Vel y: %.10lf\n", player1.velx, player1.vely);
    //nova posicao da nave 2
    printf ("\nNave 2:\n");
    printf ("Nome: %s\n", player2.nome);
    printf ("Pos x: %.10lf Pos y: %.10lf\n", player2.posx, player2.posy);
    printf ("Vel x: %.10lf Vel y: %.10lf\n", player2.velx, player2.vely);
    //checa se os projeteis ainda estao ativos

    //novas posicoes de cada projetil
    for(k=0; k < qtdProjetil; k++){ 
        printf ("\nProjétil %d:\n", k+1);
        printf ("Pos x: %.10lf Pos y: %.10lf\n",tiros[k].posx, tiros[k].posy);
        printf ("Vel x: %.10lf Vel y: %.10lf\n",tiros[k].velx, tiros[k].vely);
    }

    if (cont >= tempoTotal)
        printf("\n############### FINAL DA TABELA ###############\n");
}