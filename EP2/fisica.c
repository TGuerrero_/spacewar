//#################################################################
/*
 Nomes:                                NUSP:
 Erick Rodrigues de Santana          11222008
 Matheus Koiti Ito 	                 9296571        
 Thiago Guerrero                     11275297

------------------------------------------------------------------*/

//##################################################################

/*Implementação das fórmulas físicas usadas no projeto*/
//Implementado no EP1
#include "fisica.h"


double forcaGravitacional(double massa1, double massa2, double dist){
    double forca;
    forca = (CTE_GRAVITACIONAL*massa1*massa2);
    return (forca/(dist*dist));
}

double norma(double x, double y){
    double ret;
    ret = x*x + y*y;
    ret = sqrt(ret);
    return ret;
}

double distancia(double posX1, double posY1, double posX2, double posY2){
    double dist;
    dist = norma(posX1-posX2, posY1-posY2);
    return dist;
}

