//#################################################################
/*
 Nomes:                                NUSP:
 Erick Rodrigues de Santana          11222008
 Matheus Koiti Ito 	                 9296571        
 Thiago Guerrero                     11275297

------------------------------------------------------------------*/

//##################################################################

/*Implementação das fórmulas físicas usadas no projeto*/
//Implementado no EP1
#ifndef _FISICA_H
#define _FISICA_H

#include <math.h>

#define CTE_GRAVITACIONAL 0.0000000000667



/*
* ForcaGravitacional();
* Recebe a "massa1" do primeiro corpo, a "massa2" do segundo corpo e a "dist" entre os corpos,
* retorna o módulo da força gravitacional entre os corpos.
*/
double forcaGravitacional(double massa1, double massa2, double dist);

/*
* distancia();
* Recebe o par ordenado (posx, posy) de dois corpos
* e retorna a distancia entre eles.
*/
double distancia(double posX1, double posY1, double posX2, double posY2);

/*
* norma();
* Recebe x e y e calcula a norma do vetor (x,y).
*/
double norma(double x, double y);
#endif