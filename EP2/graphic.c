//#################################################################
/*
 Nomes:                                NUSP:
 Erick Rodrigues de Santana          11222008
 Matheus Koiti Ito 	                 9296571        
 Thiago Guerrero                     11275297

------------------------------------------------------------------*/

//##################################################################

/*Implementação da interface gráfica usada no projeto*/
//Implementado no EP2
#include "graphic.h"

char nome_nave1[39] = "./Imagens/xpm/Nave1/nave_rotacao01.xpm";
char nome_nave1_mask[40] = "./Imagens/xpm/Nave1/nave_mask01.xpm";
char nome_nave2[50] = "./Imagens/xpm/Nave2/nave2_rotacao01.xpm";
char nome_nave2_mask[50] = "./Imagens/xpm/Nave2/nave2_mask01.xpm";


void aplicaMascara(WINDOW *tela, PIC imagem, MASK mascara, int largura, int altura, int posx, int posy){
    SetMask(tela, mascara);
    PutPic(tela, imagem, 0, 0, largura, altura, posx, posy);
    UnSetMask(tela);
}

void carregaImagemNave (WINDOW *tela, PIC nave[], PIC mask[], PIC aux[], int player){
    int i;
    for (i=0; i < 16; i++){
        aux[i] = NewPic(tela,50, 85);
        mask[i] = NewMask(tela, 50, 85);

        //Modifica o nome do arquivo acessado
        if (player == 1){
            nome_nave1[32] = ((i+1) / 10) + 48;
            nome_nave1[33] = ((i+1) % 10) + 48;
            nome_nave1_mask[29] = ((i+1) / 10) + 48;
            nome_nave1_mask[30] = ((i+1) % 10) + 48;
            nave[i] = ReadPic(tela, nome_nave1, NULL);
            aux[i] = ReadPic(tela, nome_nave1_mask, mask[i]);
        }
        else if (player == 2){
            nome_nave2[33]= ((i+1) / 10) + 48;
            nome_nave2[34] = ((i+1) % 10) + 48;
            nome_nave2_mask[30] = ((i+1) / 10) + 48;
            nome_nave2_mask[31] = ((i+1) % 10) + 48;
            nave[i] = ReadPic(tela, nome_nave2, NULL);
            aux[i] = ReadPic(tela, nome_nave2_mask, mask[i]);
        }
    }
}
