//#################################################################
/*
 Nomes:                                NUSP:
 Erick Rodrigues de Santana          11222008
 Matheus Koiti Ito 	                 9296571        
 Thiago Guerrero                     11275297

------------------------------------------------------------------*/

//##################################################################

/*Implementação da interface gráfica usada no projeto*/
//Implementado no EP2
#ifndef _GRAPHIC_H
#define _GRAPHIC_H

#include "xwc.h"
#include <unistd.h>

/*
* aplicaMascara();
* Recebe a tela, uma imagem (largura x altura), a mascara dessa imagem e coloca a imagem,
* aplicando a máscara, na posição posx e posy.
*/
void aplicaMascara(WINDOW *tela, PIC imagem, MASK mascara, int largura, int altura, int posx, int posy);

/*
* carregaImagemNave();
* Recebe todos os vetores de todas as orientações de uma nave
* e inicializa todas elas.
*/
void carregaImagemNave (WINDOW *tela, PIC nave[], PIC mask[], PIC aux[], int player);

#endif