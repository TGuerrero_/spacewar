 
//#################################################################
/*
 Nomes:                                NUSP:
 Erick Rodrigues de Santana          11222008
 Matheus Koiti Ito 	                 9296571        
 Thiago Guerrero                     11275297

------------------------------------------------------------------*/

#include <string.h>
#include "fisica.h"
#include "graphic.h"
#include "cte.h"

/*######################################################################
                            Variável GLobal
######################################################################*/

nave player1, player2;
planeta terra;
projetil *tiros;
int qtdProjetil;
double duracaoProjetil;

//#####################################################################

/*Funções*/

// Funções EP1:
int input(double *tempoTotal);
void output();
void leituraEntradas(double tempoTotal);
void tabelaSaidas(int i, double cont, double tempoTotal, int tipoDeLeitura);

//#####################################################################

int main(int argc, char * argv[]) {
    //Declaração das variáveis gráficas
    WINDOW * tela;
    PIC Gbackground, GimagemOriginal, Grascunho;
    PIC Gplaneta, Gplayer1[16], Gplayer2[16], Gtiros;
    PIC GauxPlaneta, GauxPlayer1[16], GauxPlayer2[16], GauxTiros;
    MASK GplanetaMask, Gplayer1Mask[16], Gplayer2Mask[16], GtirosMask;

    //Declaração das medidas das 16 orientação das naves
    int largura[16]= {50, 78, 95, 97, 85, 97, 95, 78, 49, 78, 95, 97, 85, 97, 95, 78};
    int altura[16] = {85, 97, 95, 78, 49, 78, 95, 97, 85, 97, 95, 78, 49, 78, 95, 97};

    //Declaração de variáveis
    double tempoTotal, cont;
    double pixelPosx, pixelPosy;
    int check, i, j, escolhePic1, escolhePic2;
    int tipoDeLeitura, log;

    //Declaração de variáveis de controle do jogo
    int ganhador, statusJogo;

    

    if (argc > 1){
        if (!strcmp(argv[1], "-h")){
            printf ("\n      #Parâmetros para execução#\n");
            printf ("\n'-h' - HELP: Mostra todos os parâmetros possíveis;\n");
            printf ("'-l' - LOG: Habilita a exibição do log no terminal;\n");
            printf ("'-lp' - LOG PAUSADO: Habilita a exibição do log no terminal pausando para análise a cada 10 iterações;\n");
            return EXIT_SUCCESS;
        }
        else if(!strcmp(argv[1],"-l")){
            log = 1;
            tipoDeLeitura = 0;
        }
        else if (!strcmp(argv[1],"-lp")){
            log = 1;
            tipoDeLeitura = 1;
        }
        else{
            printf ("Parâmetro inválido! Use o parâmetro '-h' para ver a lista de parâmetros.\n");
            return EXIT_FAILURE;
        }
    }

    else{
        log = 0;
        tipoDeLeitura = 0;
    }

    printf ("\n             # SPACE WAR #             \n");
    printf ("\n             # Controle das naves #             \n");
    printf ("'->' - Rotaciona a nave 1 para a direita\n");
    printf ("'<-' - Rotaciona a nave 1 para a esquerda\n");
    printf ("'d' - Rotaciona a nave 2 para a direita\n");
    printf ("'a' - Rotaciona a nave 1 para a esquerda\n\n");

    check = input (&tempoTotal);
    if (check == EXIT_FAILURE){
        printf ("Ocorreu um erro na leitura do arquivo, tente novamente!\n");
        return EXIT_FAILURE;
    }

    terra.raio = RAIO_PLANETA;
    player1 = inicializaNave(player1);
    player2 = inicializaNave(player2);
    inicializaProjetil(tiros, qtdProjetil);


    leituraEntradas(tempoTotal);

    /****** Inicialização da parte gráfica ******/
        tela = InitGraph(TAMANHO_TELA, TAMANHO_TELA, "Space War");
        GimagemOriginal = NewPic(tela, TAMANHO_TELA, TAMANHO_TELA);
        Grascunho = NewPic(tela, TAMANHO_TELA, TAMANHO_TELA);
        Gtiros = NewPic(tela, 10, 10); 

        GauxPlaneta = NewPic(tela,125, 125);
        GauxTiros = NewPic(tela,50, 50);

        GplanetaMask = NewMask(tela, 125, 125);
        GtirosMask = NewMask(tela, 10, 10);

        carregaImagemNave(tela, Gplayer1, Gplayer1Mask, GauxPlayer1, 1);
        carregaImagemNave(tela, Gplayer2, Gplayer2Mask, GauxPlayer2, 2);

        Gbackground = ReadPic(tela, "./Imagens/xpm/space.xpm", NULL);
        Gplaneta = ReadPic(tela, "./Imagens/xpm/planeta.xpm", NULL);
        Gtiros = ReadPic(tela, "./Imagens/xpm/tiro.xpm", NULL);
        GauxPlaneta = ReadPic(tela, "./Imagens/xpm/planeta_mask.xpm", GplanetaMask);
        GauxTiros = ReadPic(tela, "./Imagens/xpm/tiro_mask.xpm", GtirosMask);

        PutPic(tela, Gbackground, 0, 0, TAMANHO_TELA, TAMANHO_TELA, 0, 0);
        aplicaMascara(tela, Gplaneta, GplanetaMask, 125, 125, (TAMANHO_TELA/2)-62.5, (TAMANHO_TELA/2)-62.5);
        PutPic(GimagemOriginal, tela, 0, 0, TAMANHO_TELA, TAMANHO_TELA, 0, 0);
        
        escolhePic1 = orientacao(player1.velx, player1.vely);
        pixelPosx = escalaNave(player1.posx, &player1)-(((double)largura[escolhePic1])/2);
        pixelPosy = escalaNave(player1.posy, &player1)-(((double)altura[escolhePic1])/2);
        aplicaMascara(tela, Gplayer1[escolhePic1], Gplayer1Mask[escolhePic1], largura[escolhePic1], altura[escolhePic1], pixelPosx, pixelPosy);
            
        escolhePic2 = orientacao(player2.velx, player2.vely);
        pixelPosx = escalaNave(player2.posx, &player2)-(((double)largura[escolhePic2])/2);
        pixelPosy = escalaNave(player2.posy, &player2)-(((double)altura[escolhePic2])/2);
        aplicaMascara(tela, Gplayer2[escolhePic2], Gplayer2Mask[escolhePic2], largura[escolhePic2], altura[escolhePic2], pixelPosx, pixelPosy);

        for (i=0; i < qtdProjetil; i++)
            aplicaMascara(tela, Gtiros, GtirosMask, 10, 10, escalaProjetil(tiros[i].posx, tiros, qtdProjetil)-5, escalaProjetil(tiros[i].posy, tiros, qtdProjetil)-5);
        PutPic(Grascunho, tela, 0, 0, TAMANHO_TELA, TAMANHO_TELA, 0, 0);
        InitKBD(tela);
    /********************************************/

    ENTER(tela);

    for (i=0, cont= 0.00, statusJogo=1; cont < tempoTotal && statusJogo == 1; cont += MAX_TIME, i++){
        //Checa se o tempo de duração dos projéteis já estourou.
        if (cont >= duracaoProjetil){
            qtdProjetil = 0;
        }

        player1 = forcaNave(player1, player2, terra, tiros, qtdProjetil);
        player2 = forcaNave(player2, player1, terra, tiros, qtdProjetil);
        forcaProjetil(player1, player2, terra, tiros, qtdProjetil);

        //Na primeira iteração, orienta os objetos na direção da força resultante.
        if (i == 0){
            if (player1.forcax < 0)
                player1.velx = -player1.velx;
            if (player1. forcay < 0)
                player1.vely = -player1.vely;

            if (player2.forcax < 0)
                player2.velx = -player2.velx;
            if (player2. forcay < 0)
                player2.vely = -player2.vely;

            for (j=0; j < qtdProjetil; j++){
                if (tiros[j].forcax < 0)
                    tiros[j].velx = -tiros[j].velx;
                if (tiros[j]. forcay < 0)
                    tiros[j].vely = -tiros[j].vely;
            }
        }

        player1 = novaVelocidadeNave(player1);
        player2 = novaVelocidadeNave(player2);
        novaVelocidadeProjetil(tiros, qtdProjetil);
        player1 = novaPosicaoNave(player1);
        player2 = novaPosicaoNave(player2);
        novaPosicaoProjetil(tiros, qtdProjetil);

        atualizaStatus(&player1, &player2, tiros, qtdProjetil);
        if (player1.hp == 0 && player2.hp == 0){
            ganhador = 0;
            statusJogo=0;
        }
        else if (player1.hp == 0){
            ganhador = 2;
            statusJogo = 0;
        }
        else if (player2.hp == 0){
            ganhador = 1;
            statusJogo = 0;
        }

        //Apaga as imagens da tela
        PutPic (Grascunho, Gbackground, 0, 0, TAMANHO_TELA, TAMANHO_TELA, 0, 0);
        aplicaMascara(Grascunho, Gplaneta, GplanetaMask, 125, 125, (TAMANHO_TELA/2)-62.5, (TAMANHO_TELA/2)-62.5);


        //Atualiza as imagens da tela
        if (player1.hp != 0){
            if (WCheckKBD(tela)){
                escolhePic1 = movimentacao(tela, escolhePic1, 1);
            }
            pixelPosx = escalaNave(player1.posx, &player1)-(((double)largura[escolhePic1])/2);
            pixelPosy = escalaNave(player1.posy, &player1)-(((double)altura[escolhePic1])/2);
            aplicaMascara(Grascunho, Gplayer1[escolhePic1], Gplayer1Mask[escolhePic1], largura[escolhePic1], altura[escolhePic1], pixelPosx, pixelPosy);
        }

        if (player2.hp != 0){
            if (WCheckKBD(tela)){
                escolhePic2 = movimentacao(tela, escolhePic2, 2);
            }
            pixelPosx = escalaNave(player2.posx, &player2)-(((double)largura[escolhePic2])/2);
            pixelPosy = escalaNave(player2.posy, &player2)-(((double)altura[escolhePic2])/2);
            aplicaMascara(Grascunho, Gplayer2[escolhePic2], Gplayer2Mask[escolhePic2],largura[escolhePic2], altura[escolhePic2], pixelPosx, pixelPosy);
        }

        for (j=0; j < qtdProjetil && tiros[j].hp != 0; j++)
            aplicaMascara(Grascunho, Gtiros, GtirosMask, 10, 10, escalaProjetil(tiros[j].posx, tiros, qtdProjetil)-5, escalaProjetil(tiros[j].posy, tiros, qtdProjetil)-5);
        PutPic (tela, Grascunho, 0, 0, TAMANHO_TELA, TAMANHO_TELA, 0, 0);

        if (log == 1)
            tabelaSaidas(i, cont+MAX_TIME, tempoTotal, tipoDeLeitura);
    }

    printf ("\nPressione qualquer tecla para sair...");
    getchar();
    getchar();

    free(tiros);
    CloseGraph();
    return 0;
}

/*Implementação - Funções*/

//################################################################################################

//################################## FUNÇÕES EP 1 ################################################

//################################################################################################

/*
* input();
* Lê todas as entradas de um arquivo.
* Recebe um ponteiro para o tempo total de simulação.
*/
int input(double *tempoTotal){
    FILE * load;
    char read[80], in[10]={"./Testes/"};
    int i;

    printf ("Digite o nome do arquivo a ser lido: ");
    scanf ("%s", read);
    strcat(in, read);
    load = fopen(in, "r");
    if (load == NULL)
        return EXIT_FAILURE;

    fscanf (load, "%lf %lf", &(terra.massa), &(*tempoTotal));
    fscanf (load, "%s %lf %lf %lf %lf %lf", &(*player1.nome), &(player1.massa), &(player1.posx), &(player1.posy), &(player1.velx), &(player1.vely));
    fscanf (load, "%s %lf %lf %lf %lf %lf", &(*player2.nome), &(player2.massa), &(player2.posx), &(player2.posy), &(player2.velx), &(player2.vely));
    fscanf (load, "%d %lf", &(qtdProjetil), &(duracaoProjetil));

    tiros = malloc(qtdProjetil * sizeof (projetil));

    for (i=0; i < qtdProjetil; i++){
        fscanf (load, "%lf %lf %lf %lf %lf", &(tiros[i].massa), &(tiros[i].posx), &(tiros[i].posy), &(tiros[i].velx), &(tiros[i].vely));
    }
    fclose (load);
    return EXIT_SUCCESS;
}

/*
* leituraEntradas();
* Recebe o tempo total de simulação e usando as variáveis globais,
* printa na tela os arquivos lidos na função "Input()".
*/
void leituraEntradas (double tempoTotal){
    int i;

    printf ("\n################# PRINT DAS ENTRADAS ##################\n");

    printf ("Raio do planeta: %.3lf\n", terra.raio); 
    printf ("Massa do planeta: %.3lf\n", terra.massa); 
    printf ("Tempo de duração: %.5lf\n", tempoTotal);

    printf ("\nNave 1:\n");
    printf ("Nome: %s\n", player1.nome);
    printf ("Massa: %.3lf\n", player1.massa);
    printf ("Pos x: %.6lf Pos y: %.6lf\n", player1.posx, player1.posy);
    printf ("Vel x: %.6lf Vel y: %.6lf\n", player1.velx,player1.vely);

    printf ("\nNave 2:\n");
    printf ("Nome: %s\n", player2.nome);
    printf ("Massa: %.3lf\n", player2.massa);
    printf ("Pos x: %.10lf Pos y: %.10lf\n", player2.posx, player2.posy);
    printf ("Vel x: %.10lf Vel y: %.10lf\n", player2.velx,player2.vely);

    printf ("Quantidade de projéteis: %d\n", qtdProjetil);
    printf ("Duração dos projéteis: %.5lf\n", duracaoProjetil);

    for(i=0; i < qtdProjetil; i++){ 
        printf ("\nProjétil %d:\n", i+1);
        printf ("Massa: %.3lf\n",tiros[i].massa);
        printf ("Pos x: %.10lf Pos y: %.10lf\n",tiros[i].posx, tiros[i].posy);
        printf ("Vel x: %.10lf Vel y: %.10lf\n",tiros[i].velx, tiros[i].vely);
    }
}

/*
* tabelaSaidas();
* Recebe a iteração "i" atual, o valor "cont" do contador atual e
* o tempo total de simulação. Usando as variáveis globais,
* printa na tela os dados dos corpos na iteração "i".
*/
void tabelaSaidas(int i, double cont, double tempoTotal, int tipoDeLeitura){
    /*Função que imprime as informações durante as interações do planeta, das naves e dos projeteis.*/
    int k;

    if (i%10 == 0 && i != 0 && tipoDeLeitura == 1){
        printf("\nAperte qualquer tecla para prosseguir.");
        getchar();
    }
    
    printf ("\n##############  %4.3lf milésimos após o início ###############\n",cont);
    //nova posicao da nave 1
    printf ("Nave 1:\n");
    printf ("Nome: %s\n", player1.nome);
    printf ("Pos x: %.10lf Pos y: %.10lf\n", player1.posx, player1.posy);
    printf ("Vel x: %.10lf Vel y: %.10lf\n", player1.velx, player1.vely);
    //nova posicao da nave 2
    printf ("\nNave 2:\n");
    printf ("Nome: %s\n", player2.nome);
    printf ("Pos x: %.10lf Pos y: %.10lf\n", player2.posx, player2.posy);
    printf ("Vel x: %.10lf Vel y: %.10lf\n", player2.velx, player2.vely);
    //checa se os projeteis ainda estao ativos

    //novas posicoes de cada projetil
    for(k=0; k < qtdProjetil; k++){ 
        printf ("\nProjétil %d:\n", k+1);
        printf ("Pos x: %.10lf Pos y: %.10lf\n",tiros[k].posx, tiros[k].posy);
        printf ("Vel x: %.10lf Vel y: %.10lf\n",tiros[k].velx, tiros[k].vely);
    }

    if (cont >= tempoTotal)
        printf("\n############### FINAL DA TABELA ###############\n");
}

//################################################################################################

//################################## FUNÇÕES EP 3 ################################################

//################################################################################################

/* Se precisarem fazer alguma função que não encaixe em nenhum módulo façam elas aqui*/