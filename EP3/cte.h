//#################################################################
/*
 Nomes:                                NUSP:
 Erick Rodrigues de Santana          11222008
 Matheus Koiti Ito 	                 9296571        
 Thiago Guerrero                     11275297

//##################################################################

/*Declaração das constantes usadas no projeto*/
//Implementado no EP3

#define EXIT_FAILURE 1
#define EXIT_SUCCESS 0
#define MAX_TIME /*0.001*/0.5
#define RAIO_PLANETA 6300
#define T_TOROIDE 4*RAIO_PLANETA
#define TAMANHO_TELA 800
#define CTE_GRAVITACIONAL 0.0000000000667

#define RAIO_NAVE_PX 42.5 // 80/2
#define RAIO_PROJETIL_PX 5 // 10/2
#define RAIO_PLANETA_PX 62.5 // 125/2 
