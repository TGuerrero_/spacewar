//#################################################################
/*
 Nomes:                                NUSP:
 Erick Rodrigues de Santana          11222008
 Matheus Koiti Ito 	                 9296571        
 Thiago Guerrero                     11275297

------------------------------------------------------------------*/

#include "fisica.h"

//##################################################################

/*Implementação da parte física no projeto*/

//Formulas
double forcaGravitacional(double massa1, double massa2, double dist){
    double forca;
    forca = (CTE_GRAVITACIONAL*massa1*massa2);
    return (forca/(dist*dist));
}

double norma(double x, double y){
    double ret;
    ret = x*x + y*y;
    ret = sqrt(ret);
    return ret;
}

double distancia(double posX1, double posY1, double posX2, double posY2){
    double dist;
    dist = norma(posX1-posX2, posY1-posY2);
    return dist;
}

int referencial(double pos1, double pos2){
    int check;
    if (pos1 - pos2 >= 0)
        check = 1;
    else
        check = -1;

    return check;
}

int converte(double pos, int tipo){
    double calculoPosicao;
    int novaPosicao;
    if (tipo == 1){ //De (x,y) para (xPx, yPx)
        calculoPosicao = pos + T_TOROIDE;
        calculoPosicao = (calculoPosicao*TAMANHO_TELA)/(2*T_TOROIDE);
        novaPosicao = ((int)calculoPosicao);
    }
    else if (tipo == 2){ //De (xPx, yPx) para (x,y)
        calculoPosicao = pos * 2 *T_TOROIDE/TAMANHO_TELA;
        calculoPosicao = calculoPosicao - T_TOROIDE;
        novaPosicao = ((int)calculoPosicao);
    }
    return novaPosicao;
}



//Cálculos
nave forcaNave(nave player, nave secundaria, planeta terra, projetil *tiros, int qtdProjeteis){
    int i, direcao;
    double dist, seno, cosseno;
    double forcaxLocal, forcayLocal, forcax=0, forcay=0, forcaGrav;

    //Calculo da força em relação ao planeta
    dist = distancia(player.posx, player.posy, 0, 0);
    forcaGrav = forcaGravitacional(terra.massa, player.massa, dist);
    seno = abs(player.posy-0)/dist; 
    cosseno = abs(player.posx-0)/dist;

    direcao = referencial(0, player.posx);
    forcaxLocal = direcao * forcaGrav * cosseno;
    direcao = referencial(0, player.posy);
    forcayLocal = direcao * forcaGrav * seno;
    forcax += forcaxLocal;
    forcay += forcayLocal;

    //Calculo da força em relação aos projéteis
    for (i=0; i < qtdProjeteis; i++){
        dist = distancia(player.posx, player.posy, tiros[i].posx, tiros[i].posy);
        forcaGrav = forcaGravitacional(player.massa, tiros[i].massa, dist);
        seno = abs(player.posy - tiros[i].posy)/dist;
        cosseno = abs(player.posx - tiros[i].posx)/dist;

        direcao = referencial(tiros[i].posx, player.posx);
        forcaxLocal = direcao * forcaGrav * cosseno;
        direcao = referencial(tiros[i].posy, player.posy);
        forcayLocal = direcao * forcaGrav * seno;

        forcax += forcaxLocal;
        forcay += forcayLocal;
    }

    //Calculo da força em relação à nave secundária
    dist = distancia(player.posx, player.posy, secundaria.posx, secundaria.posy);
    forcaGrav = forcaGravitacional(player.massa, secundaria.massa, dist);
    seno = abs(secundaria.posy - player.posy)/dist;
    cosseno = abs(secundaria.posx - player.posx)/dist;

    direcao = referencial(secundaria.posx, player.posx);
    forcaxLocal = direcao * forcaGrav * cosseno;
    direcao = referencial(secundaria.posy, player.posy);
    forcayLocal = direcao * forcaGrav * seno;

    forcax += forcaxLocal;
    forcay += forcayLocal;

    //Armazenamento da força resultante
    player.forcax = forcax;
    player.forcay = forcay;
    return player;
}

nave novaPosicaoNave(nave player){
    double posFinal, aceleracaox, aceleracaoy;

    aceleracaox = player.forcax/player.massa;
    aceleracaoy = player.forcay/player.massa;
    
    posFinal = player.posx + (player.velx*MAX_TIME)+ ((aceleracaox*(MAX_TIME)*(MAX_TIME))/2);
    player.posx = posFinal;
    posFinal = player.posy + (player.vely*MAX_TIME)+ ((aceleracaoy*(MAX_TIME)*(MAX_TIME))/2);
    player.posy = posFinal;

    return player;
}

nave novaVelocidadeNave(nave player){
    double velFinal, aceleracaox, aceleracaoy;
    
    aceleracaox = player.forcax/player.massa;
    aceleracaoy = player.forcay/player.massa;

    velFinal = player.velx + (aceleracaox*MAX_TIME);
    player.velx = velFinal;
    velFinal = player.vely + (aceleracaoy*MAX_TIME);
    player.vely = velFinal;

    return player;
}

int escalaNave(double pos, nave *player){
    int novaPosicao;

    novaPosicao = converte(pos, 1);
    if ((*player).posx > T_TOROIDE)
        (*player).posx = -T_TOROIDE;
    if ((*player).posy > T_TOROIDE)
        (*player).posy = -T_TOROIDE;
    if ((*player).posx < -T_TOROIDE)
        (*player).posx = T_TOROIDE;
    if ((*player).posy < -T_TOROIDE)
        (*player).posy = T_TOROIDE;

    return novaPosicao%TAMANHO_TELA;
}

void forcaProjetil(nave player1, nave player2, planeta terra, projetil *tiros, int qtdProjeteis){
    int i, j, direcao;
    double dist, seno, cosseno;
    double forcaxLocal, forcayLocal, forcax=0, forcay=0, forcaGrav;

    for (i = 0; i < qtdProjeteis; i++, forcax=0,forcay=0){
        // Calculo da força em relação ao planeta
        dist = distancia(tiros[i].posx, tiros[i].posy, 0, 0);
        forcaGrav = forcaGravitacional(terra.massa, tiros[i].massa, dist);
        seno = abs(tiros[i].posy-0)/dist; 
        cosseno = abs(tiros[i].posx)-0/dist;

        direcao = referencial(0, tiros[i].posx);
        forcaxLocal = direcao * forcaGrav * cosseno;
        direcao = referencial(0, tiros[i].posy);
        forcayLocal = direcao * forcaGrav * seno;
        forcax += forcaxLocal;
        forcay += forcayLocal;

        // Calculo da força em relação à primeira nave
        dist = distancia(tiros[i].posx, tiros[i].posy, player1.posx, player1.posy);
        forcaGrav = forcaGravitacional(tiros[i].massa, player1.massa, dist);
        seno = abs(tiros[i].posy - player1.posy)/dist;
        cosseno = abs(tiros[i].posx - player1.posx)/dist;

        direcao = referencial(player1.posx, tiros[i].posx);
        forcaxLocal = direcao * forcaGrav * cosseno;
        direcao = referencial(player1.posy, tiros[i].posy);
        forcayLocal = direcao * forcaGrav * seno;

        forcax += forcaxLocal;
        forcay += forcayLocal;

        // Calculo da força em relação à segunda nave
        dist = distancia(tiros[i].posx, tiros[i].posy, player2.posx, player2.posy);
        forcaGrav = forcaGravitacional(tiros[i].massa, player2.massa, dist);
        seno = abs(tiros[i].posy - player2.posy)/dist;
        cosseno = abs(tiros[i].posx - player2.posx)/dist;

        direcao = referencial(player2.posx, tiros[i].posx);
        forcaxLocal = direcao * forcaGrav * cosseno;
        direcao = referencial(player2.posy, tiros[i].posy);
        forcayLocal = direcao * forcaGrav * seno;

        forcax += forcaxLocal;
        forcay += forcayLocal;

        // Calculo da força que os projeteis restantes exercem
        for (j = 0; j < qtdProjeteis; j++){
            if (j == i)
                continue;
            
            dist = distancia(tiros[i].posx, tiros[i].posy, tiros[j].posx, tiros[j].posy);
            forcaGrav = forcaGravitacional(tiros[i].massa, tiros[j].massa, dist);
            seno = abs(tiros[i].posy - tiros[j].posy)/dist;
            cosseno = abs(tiros[i].posx - tiros[j].posx)/dist;

            direcao = referencial(tiros[j].posx, tiros[i].posx);
            forcaxLocal = direcao * forcaGrav * cosseno;
            direcao = referencial(tiros[j].posy, tiros[i].posy);
            forcayLocal = direcao * forcaGrav * seno;

            forcax += forcaxLocal;
            forcay += forcayLocal;
        }
        //Armazenamento da força resultante
        tiros[i].forcax = forcax;
        tiros[i].forcay = forcay; 
    }
}

void novaPosicaoProjetil(projetil *tiros, int qtdProjeteis){
    int i;
    double posFinal, aceleracaox, aceleracaoy;
    for (i=0; i < qtdProjeteis; i++){
        aceleracaox = tiros[i].forcax/tiros[i].massa;
        aceleracaoy = tiros[i].forcay/tiros[i].massa;

        posFinal = tiros[i].posx + (tiros[i].velx*MAX_TIME)+ ((aceleracaox*(MAX_TIME)*(MAX_TIME))/2);
        tiros[i].posx = posFinal;
        posFinal = tiros[i].posy + (tiros[i].vely*MAX_TIME)+ ((aceleracaoy*(MAX_TIME)*(MAX_TIME))/2);
        tiros[i].posy = posFinal;
    }
}

void novaVelocidadeProjetil(projetil *tiros, int qtdProjeteis){
    int i;
    double velFinal, aceleracaox, aceleracaoy;
    for (i=0; i < qtdProjeteis; i++){
        aceleracaox = tiros[i].forcax/tiros[i].massa;
        aceleracaoy = tiros[i].forcay/tiros[i].massa;

        velFinal = tiros[i].velx + (aceleracaox*MAX_TIME);
        tiros[i].velx = velFinal;
        velFinal = tiros[i].vely + (aceleracaoy*MAX_TIME);
        tiros[i].vely = velFinal;
    }
}

int escalaProjetil(double pos, projetil *tiros, int qtdProjeteis){
    int novaPosicao, i;

    novaPosicao = converte(pos, 1);

    for (i=0; i < qtdProjeteis; i++){
        if (tiros[i].posx > T_TOROIDE)
            tiros[i].posx = -T_TOROIDE;
        if (tiros[i].posy > T_TOROIDE)
            tiros[i].posy = -T_TOROIDE;
        if (tiros[i].posx < -T_TOROIDE)
            tiros[i].posx = T_TOROIDE;
        if (tiros[i].posy < -T_TOROIDE)
            tiros[i].posy = T_TOROIDE;
    }
    return novaPosicao%TAMANHO_TELA;
}



//Controle de jogo
nave inicializaNave(nave player){
    player.hp = 100;
    return player;
}

void inicializaProjetil(projetil *tiros, int qtdProjeteis){
    int i;

    for (i=0; i < qtdProjeteis; i++)
        tiros[i].hp = 100;
}

int colisao(nave player1, nave player2, projetil *tiros, int qtdProjeteis, int objeto){
    int i;
    double dist, distMin, posx, posy;
    double raioNave, raioPlaneta, raioTiros;

    raioNave = distancia(converte(0, 2), converte(0, 2), converte(RAIO_NAVE_PX, 2), converte(0, 2));
    raioNave = abs(raioNave);

    raioPlaneta = distancia(converte(0, 2), converte(0, 2), converte(RAIO_PLANETA_PX, 2) , converte(0, 2));
    raioPlaneta = abs(raioPlaneta);

    raioTiros = distancia(converte(0, 2), converte(0, 2), converte(RAIO_PROJETIL_PX, 2) , converte(0, 2));
    raioTiros = abs(raioTiros);

    if (objeto == 1){
        posx = player1.posx;
        posy = player1.posy;
    }
    if (objeto == 2){
        posx = player2.posx;
        posy = player2.posy;
    }

    //Checa se as naves colidiram entre si
    distMin = 2 * raioNave;
    dist = distancia(player1.posx, player1.posy, player2.posx, player2.posy);
    if (dist <= distMin)
        return -1;
    //Checa se as naves colidiram com os projéteis
    distMin = raioNave + raioTiros;
    for (i=0; i < qtdProjeteis; i++){
            dist = distancia(posx, posy, tiros[i].posx, tiros[i].posy);
        if (dist <= distMin)
            return i;
    }
    //Checa se as naves colidiram com o planeta
    distMin = raioNave + raioPlaneta;
    dist = distancia(posx, posy, 0, 0);
    if (dist <= distMin)
        return -10;

    return 0;
}

void atualizaStatus(nave *player1, nave *player2, projetil *tiros, int qtdProjeteis){
    int i;
    switch ((i = colisao(*player1, *player2, tiros, qtdProjeteis, 1))){
        case -1:
            (*player1).hp = 0;
            (*player2).hp = 0;
            break;

        case -10:
            (*player1).hp = 0;
            break;

        case 0:
            break;

        default:
            tiros[i].hp = 0;
            (*player1).hp = 0;
            break;
    }

    switch ((i = colisao(*player1, *player2, tiros, qtdProjeteis, 2))){
        case -1:
            (*player1).hp = 0;
            (*player2).hp = 0;
            break;

        case -10:
            (*player2).hp = 0;
            break;

        case 0:
            break;

        default:
            tiros[i].hp = 0;
            (*player2).hp = 0;
            break;
    }
}