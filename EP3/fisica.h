//#################################################################
/*
 Nomes:                                NUSP:
 Erick Rodrigues de Santana          11222008
 Matheus Koiti Ito 	                 9296571        
 Thiago Guerrero                     11275297

------------------------------------------------------------------*/

#ifndef _FISICA_H
#define _FISICA_H

#include <math.h>
#include <stdlib.h>
#include "cte.h"
#include <stdio.h>

typedef struct s_nave nave;
typedef struct s_planeta planeta;
typedef struct s_projetil projetil;

struct s_nave{
    char nome[80];
    double massa;
    double posx, posy;
    double velx, vely;
    double forcax, forcay;
    int hp;
};

struct s_planeta{
    double massa;
    double raio;
};

struct s_projetil{
    double massa;
    double posx, posy;
    double velx, vely;
    double forcax, forcay;
    int hp;
};

//##################################################################

/*Implementação da parte física no projeto*/
//Implementado no EP1

//Formulas
/*
* ForcaGravitacional();
* Recebe a "massa1" do primeiro corpo, a "massa2" do segundo corpo e a "dist" entre os corpos,
* retorna o módulo da força gravitacional entre os corpos.
*/
double forcaGravitacional(double massa1, double massa2, double dist);

/*
* distancia();
* Recebe o par ordenado (posx, posy) de dois corpos
* e retorna a distancia entre eles.
*/
double distancia(double posX1, double posY1, double posX2, double posY2);

/*
* norma();
* Recebe x e y e calcula a norma do vetor (x,y).
*/
double norma(double x, double y);

/*
* referencial();
* Recebe a posição x ou y do corpo 1 e do corpo 2 e retorna:
*  1 - Se o referencial for negativo;
* -1 - Se o referecial for positivo.
*
* OBS: Pos1 será sempre o corpo gerador da força;
* Pos2 será o corpo no qual a força é aplicada.
*/
int referencial(double pos1, double pos2);

/*
* converte();
* Recebe uma posição x, y, xPx ou yPx dos eixos cartesianos e retorna  
* a sua conversão para a outra unidade;
* Se tipo == 1 converte de cartesiano para pixel;
* Se tipo == 2 converte de pixel para cartesiano;
* A função usa a seguinte fórmula:
* X = X' + Tam. do toroide * (Tam. da tela/(2*Tam. do toroide))
*/
int converte(double pos, int tipo);




//Cálculos
/*
* ForcaNave();
* Faz o cálculo da força resultante na Nave "player" e armazena na própria struct. 
* Recebe como parâmetro as structs das naves, dos projéteis e da terra.
*/
nave forcaNave(nave player, nave secundaria, planeta terra, projetil *tiros, int qtdProjeteis);

/*
* novaPosicaoNave();
* Usando a força resultante e as novas velocidades da nave,
* calcula a nova posição da nave.
* Recebe a nave que será atualizada.
*/
nave novaPosicaoNave(nave player);

/*
* novaVelocidadeNave();
* Usando a força resultante da nave, calcula a sua nova velocidade.
* Recebe a nave que será atualizada.
*/
nave novaVelocidadeNave(nave player);

/*
* escalaNave();
* Recebe uma posição(x ou y) dos eixos cartesianos e retorna a posição xPx ou yPx,
* na escala correta, em pixels. A função aplica o toroide nas posições necessárias.
*/
int escalaNave(double pos, nave *player);

/*
* ForcaProjetil();
* Faz o cálculo da força resultante nos "qtdProjetil" projéteis e armazena na própria struct, 
* recebe como parâmetro as structs das naves, dos projéteis e da terra.
*/
void forcaProjetil(nave player1, nave player2, planeta terra, projetil *tiros, int qtdProjeteis);

/*
* novaPosicaoProjetil();
* Usando a força resultante e as novas velocidades dos projéteis,
* calcula a nova posição deles.
* Recebe o vetor de projéteis.
*/
void novaPosicaoProjetil(projetil * tiros, int qtdProjeteis);

/*
* novaVelocidadeProjetil();
* Usando a força resultante dos projéteis, calcula as suas novas velocidades.
* Recebe a nave que será atualizada.
*/
void novaVelocidadeProjetil(projetil *tiros, int qtdProjeteis);

/*
* escalaNave();
* Recebe uma posição(x ou y) dos eixos cartesianos e retorna a posição xPx ou yPx,
* na escala correta, em pixels. A função aplica o toroide nas posições necessárias.
*/
int escalaProjetil(double pos, projetil *tiros, int qtdProjeteis);




//Implementado no EP3

//Controle de jogo
/*
* inicializaNave();
* Recebe a nave e inicializa todos os campos
* de status do objeto.
*/
nave inicializaNave(nave player);

/*
* inicializaProjetil();
* Recebe os projeteis e inicializa todos os campos
* de status dos objetos.
*/
void inicializaProjetil(projetil *tiros, int qtdProjeteis);

/*
* colisao();
* Recebe os dados dos corpos e através do parâmetro "objeto" calcula:
* 1- As colisões no player 1;
* 2- As colisões no player 2.
* Retorna:
* -1 se colidiu com outra nave;
* -10 se colidiu com o planeta;
* n se colidiu com o n-ésimo projétil {n = [0, qtdProjeteis]};
* 0 se não colidiu.
*/
int colisao(nave player1, nave player2, projetil *tiros, int qtdProjeteis, int objeto);

/*
* atualizaStatus();
* Recebe os objetos e, através da chamada da função colisao(),
* atualiza os campos de status dos objetos.
*/
void atualizaStatus(nave *player1, nave *player2, projetil *tiros, int qtdProjeteis);
#endif