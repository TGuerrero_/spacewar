//#################################################################
/*
 Nomes:                                NUSP:
 Erick Rodrigues de Santana          11222008
 Matheus Koiti Ito 	                 9296571        
 Thiago Guerrero                     11275297

------------------------------------------------------------------*/

#ifndef _GRAPHIC_H
#define _GRAPHIC_H

#include "xwc.h"
#include "fisica.h"
#include "/usr/include/X11/keysym.h"
#include <unistd.h>
#include <stdio.h>

//##################################################################

/*Implementação da interface gráfica usada no projeto*/
//Implementado no EP2

/*
* aplicaMascara();
* Recebe a tela, uma imagem (largura x altura), a mascara dessa imagem e coloca a imagem,
* aplicando a máscara, na posição posx e posy.
*/
void aplicaMascara(WINDOW *tela, PIC imagem, MASK mascara, int largura, int altura, int posx, int posy);

/*
* carregaImagemNave();
* Recebe todos os vetores de todas as orientações de uma nave
* e inicializa todas elas.
*/
void carregaImagemNave (WINDOW *tela, PIC nave[], PIC mask[], PIC aux[], int player);

/*
* orientacao();
* Recebe o vetor velocidade (velx, vely) e
* retorna uma orientacao pra nave.
*/
int orientacao(double velx, double vely);

/*
* ENTER();
* Pausa a execução até a tecla "ENTER" ser pressionada.
*/
void ENTER (WINDOW *tela);




//Controle de jogo
/*
* movimentacao();
* Rece a tela e a orientação atual e retorna a nova orientação
* após o acionamento das teclas de rotação no jogo.
*/
int movimentacao (WINDOW *tela, int imagemAtual, int nave);
#endif