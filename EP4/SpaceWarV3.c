 
//#################################################################
/*
 Nomes:                                NUSP:
 Erick Rodrigues de Santana          11222008
 Matheus Koiti Ito 	                 9296571        
 Thiago Guerrero                     11275297

------------------------------------------------------------------*/

#include "fisica.h"
#include "graphic.h"
#include "cte.h"
#include "io.h"

/*######################################################################
                            Variáveis Globais
######################################################################*/

nave player1, player2;
planeta terra;
projetil *tiros;

//#####################################################################

int main(int argc, char * argv[]) {
    //Declaração das variáveis gráficas
    WINDOW * tela;
    PIC Gbackground, GimagemOriginal, Grascunho;
    PIC GmenuInicial[3], GcomoJogar, GfimDeJogo[6];
    PIC Gplaneta, Gplayer1[16], Gplayer2[16], Gtiros, GscoreP1[3], GscoreP2[3];
    PIC GauxPlaneta, GauxPlayer1, GauxPlayer2, GauxTiros, GauxScore;
    MASK GplanetaMask, Gplayer1Mask[16], Gplayer2Mask[16], GtirosMask, GscoreMask[3];
    KeyCode keyAtual;

    //Declaração das medidas das 16 orientação das naves
    int largura[16]= {85, 97, 95, 78, 50, 78, 95, 97, 85, 97, 95, 78, 49, 78, 95, 97};
    int altura[16] = {49, 78, 95, 97, 85, 97, 95, 78, 49, 78, 95, 97, 85, 97, 95, 78};

    //Declaração de variáveis
    double cont, delayNave1, delayNave2;
    double pixelPosx, pixelPosy;
    int check, i, escolhePic1, escolhePic2;
    int log;

    //Declaração de variáveis de controle do jogo
    int ganhador, statusJogo, jogarNovamente=1;
    projetil* aux;

    check = parametros(argc, argv, &log);
    if (check == 2 || check == EXIT_FAILURE)
        return check;

    /****** Inicialização da parte gráfica ******/
        tela = InitGraph(TAMANHO_TELA, TAMANHO_TELA, "Space War");
        GimagemOriginal = NewPic(tela, TAMANHO_TELA, TAMANHO_TELA);
        Grascunho = NewPic(tela, TAMANHO_TELA, TAMANHO_TELA);
        Gtiros = NewPic(tela, 10, 10); 

        GauxPlaneta = NewPic(tela,125, 125);
        GauxTiros = NewPic(tela,50, 50);

        GplanetaMask = NewMask(tela, 125, 125);
        GtirosMask = NewMask(tela, 10, 10);

        carregaMenu(tela, GmenuInicial, GfimDeJogo, &GcomoJogar);
        carregaScore(tela, GscoreP1, GscoreP2, GscoreMask, GauxScore);
        carregaImagemNave(tela, Gplayer1, Gplayer1Mask, GauxPlayer1, 1);
        carregaImagemNave(tela, Gplayer2, Gplayer2Mask, GauxPlayer2, 2);

        Gbackground = ReadPic(tela, "./Imagens/xpm/space.xpm", NULL);
        Gplaneta = ReadPic(tela, "./Imagens/xpm/planeta.xpm", NULL);
        Gtiros = ReadPic(tela, "./Imagens/xpm/tiro.xpm", NULL);
        GauxPlaneta = ReadPic(tela, "./Imagens/xpm/planeta_mask.xpm", GplanetaMask);
        GauxTiros = ReadPic(tela, "./Imagens/xpm/tiro_mask.xpm", GtirosMask);
        InitKBD(tela);
    /********************************************/
    while(jogarNovamente){
        printf ("\n             # SPACE WAR #             \n");
        check = input (&terra, &player1, &player2);
        if (check == EXIT_FAILURE){
            printf ("Ocorreu um erro na leitura do arquivo, tente novamente!\n");
            return EXIT_FAILURE;
        }

        terra.raio = RAIO_PLANETA;
        tiros = projetilInit();
        leituraEntradas(terra, player1, player2);


        /****** Aplicação da parte gráfica ******/
            PutPic(tela, Gbackground, 0, 0, TAMANHO_TELA, TAMANHO_TELA, 0, 0);
            aplicaMascara(tela, Gplaneta, GplanetaMask, 125, 125, (TAMANHO_TELA/2)-62.5, (TAMANHO_TELA/2)-62.5);
            PutPic(GimagemOriginal, tela, 0, 0, TAMANHO_TELA, TAMANHO_TELA, 0, 0);
            
            escolhePic1 = orientacao(player1.velx, player1.vely);
            pixelPosx = escalaNave(player1.posx, &player1)-(((double)largura[escolhePic1])/2);
            pixelPosy = escalaNave(player1.posy, &player1)-(((double)altura[escolhePic1])/2);
            aplicaMascara(tela, Gplayer1[escolhePic1], Gplayer1Mask[escolhePic1], largura[escolhePic1], altura[escolhePic1], pixelPosx, pixelPosy);
                
            escolhePic2 = orientacao(player2.velx, player2.vely);
            pixelPosx = escalaNave(player2.posx, &player2)-(((double)largura[escolhePic2])/2);
            pixelPosy = escalaNave(player2.posy, &player2)-(((double)altura[escolhePic2])/2);
            aplicaMascara(tela, Gplayer2[escolhePic2], Gplayer2Mask[escolhePic2], largura[escolhePic2], altura[escolhePic2], pixelPosx, pixelPosy);
            
            PutPic(Grascunho, tela, 0, 0, TAMANHO_TELA, TAMANHO_TELA, 0, 0);
        /********************************************/

        if (!menuInicial(tela, GmenuInicial, GcomoJogar)){
            //Botão "Sair"
            projetilFree(tiros);
            CloseGraph();
            return 0;
        }
    
        delayNave1 = delayNave2 = 0;
        for (i=0, cont= 0.00, statusJogo=1; statusJogo; cont += MAX_TIME, i++){
            delayNave1 -= MAX_TIME;
            delayNave2 -= MAX_TIME;
            //Checa se algum projétil já passou do seu tempo de vida
            duracaoProjetil(tiros, cont);
            //Checa se alguma tecla foi apertada
            if (WCheckKBD(tela)){
                keyAtual = WGetKey(tela);
                if (keyAtual == 111) //Seta pra cima
                    player1 = aceleracaoNave(player1, escolhePic1);
                else if (keyAtual == 25) //W
                    player2 = aceleracaoNave(player2, escolhePic2);
                else if (keyAtual == 116 && delayNave1 <= 0){ //Seta pra baixo
                    criaProjetil(tiros, escolhePic1, player1.posx, player1.posy, cont);
                    delayNave1 = 80;
                }
                else if (keyAtual == 39 && delayNave2 <= 0){ //S
                    criaProjetil(tiros, escolhePic2, player2.posx, player2.posy, cont);
                    delayNave2 = 80;
                }
                else if (keyAtual == 114 || keyAtual == 113) //Seta pra direita ou para esquerda
                    escolhePic1 = movimentacao(keyAtual, escolhePic1, 1);
                else //D ou A
                    escolhePic2 = movimentacao(keyAtual, escolhePic2, 2);
            }

            player1 = forcaNave(player1, player2, terra, tiros);
            player2 = forcaNave(player2, player1, terra, tiros);
            forcaProjetil(player1, player2, terra, tiros);

            //Na primeira iteração, orienta os objetos na direção da força resultante.
            if (i == 0){
                if (player1.forcax < 0)
                    player1.velx = -player1.velx;
                if (player1. forcay < 0)
                    player1.vely = -player1.vely;

                if (player2.forcax < 0)
                    player2.velx = -player2.velx;
                if (player2. forcay < 0)
                    player2.vely = -player2.vely;
            }

            player1 = novaVelocidadeNave(player1);
            player2 = novaVelocidadeNave(player2);
            novaVelocidadeProjetil(tiros);
            player1 = novaPosicaoNave(player1);
            player2 = novaPosicaoNave(player2);
            novaPosicaoProjetil(tiros);

            colisao(&player1, &player2, tiros);
            atualizaStatus(&ganhador, &statusJogo, player1, player2);

            //Apaga as imagens da tela
            PutPic (Grascunho, Gbackground, 0, 0, TAMANHO_TELA, TAMANHO_TELA, 0, 0);
            aplicaMascara(Grascunho, Gplaneta, GplanetaMask, 125, 125, (TAMANHO_TELA/2)-62.5, (TAMANHO_TELA/2)-62.5);


            //Atualiza as imagens da tela
            if (player1.hp != 0){
                pixelPosx = escalaNave(player1.posx, &player1)-(((double)largura[escolhePic1])/2);
                pixelPosy = escalaNave(player1.posy, &player1)-(((double)altura[escolhePic1])/2);
                aplicaMascara(Grascunho, Gplayer1[escolhePic1], Gplayer1Mask[escolhePic1], largura[escolhePic1], altura[escolhePic1], pixelPosx, pixelPosy);
            }

            if (player2.hp != 0){
                pixelPosx = escalaNave(player2.posx, &player2)-(((double)largura[escolhePic2])/2);
                pixelPosy = escalaNave(player2.posy, &player2)-(((double)altura[escolhePic2])/2);
                aplicaMascara(Grascunho, Gplayer2[escolhePic2], Gplayer2Mask[escolhePic2],largura[escolhePic2], altura[escolhePic2], pixelPosx, pixelPosy);
            }

            aux = tiros->prox;
            while (aux != NULL){
                aplicaMascara(Grascunho, Gtiros, GtirosMask, 10, 10, escalaProjetil(aux->posx, tiros)-5, escalaProjetil(aux->posy, tiros)-5);
                aux = aux->prox;
            }

            scoreVida(Grascunho, player1, player2, GscoreP1, GscoreP2, GscoreMask);
            PutPic (tela, Grascunho, 0, 0, TAMANHO_TELA, TAMANHO_TELA, 0, 0);

            if (log == 1)
                tabelaSaidas(cont+MAX_TIME, terra, player1, player2, tiros);
        }

        projetilFree(tiros);
        if (fimDeJogo(tela, ganhador, GfimDeJogo)){
            CloseGraph();
            return 0;
        }
    }

    CloseGraph();
    return 0;
}