//#################################################################
/*
 Nomes:                                NUSP:
 Erick Rodrigues de Santana          11222008
 Matheus Koiti Ito 	                 9296571        
 Thiago Guerrero                     11275297

------------------------------------------------------------------*/

#include "fisica.h"

//##################################################################

/*Implementação da parte física no projeto*/

//Formulas
double forcaGravitacional(double massa1, double massa2, double dist){
    double forca;
    forca = (CTE_GRAVITACIONAL*massa1*massa2);
    return (forca/(dist*dist));
}

double norma(double x, double y){
    double ret;
    ret = x*x + y*y;
    ret = sqrt(ret);
    return ret;
}

double distancia(double posX1, double posY1, double posX2, double posY2){
    double dist;
    dist = norma(posX1-posX2, posY1-posY2);
    return dist;
}

int referencial(double pos1, double pos2){
    int check;
    if (pos1 - pos2 >= 0)
        check = 1;
    else
        check = -1;

    return check;
}

int converte(double pos, int tipo){
    double calculoPosicao;
    int novaPosicao;
    if (tipo == 1){ //De (x,y) para (xPx, yPx)
        calculoPosicao = pos + T_TOROIDE;
        calculoPosicao = (calculoPosicao*TAMANHO_TELA)/(2*T_TOROIDE);
        novaPosicao = ((int)calculoPosicao);
    }
    else if (tipo == 2){ //De (xPx, yPx) para (x,y)
        calculoPosicao = pos * 2 *T_TOROIDE/TAMANHO_TELA;
        calculoPosicao = calculoPosicao - T_TOROIDE;
        novaPosicao = ((int)calculoPosicao);
    }
    return novaPosicao;
}



//Cálculos
nave forcaNave(nave player, nave secundaria, planeta terra, projetil *tiros){
    int direcao;
    double dist, seno, cosseno;
    double forcaxLocal, forcayLocal, forcax=0, forcay=0, forcaGrav;
    projetil *aux;

    //Calculo da força em relação ao planeta
    dist = distancia(player.posx, player.posy, 0, 0);
    forcaGrav = forcaGravitacional(terra.massa, player.massa, dist);
    seno = abs(player.posy-0)/dist; 
    cosseno = abs(player.posx-0)/dist;

    direcao = referencial(0, player.posx);
    forcaxLocal = direcao * forcaGrav * cosseno;
    direcao = referencial(0, player.posy);
    forcayLocal = direcao * forcaGrav * seno;
    forcax += forcaxLocal;
    forcay += forcayLocal;

    //Calculo da força em relação aos projéteis
    aux = tiros->prox;
    while(aux != NULL){
        dist = distancia(player.posx, player.posy, aux->posx, aux->posy);
        forcaGrav = forcaGravitacional(player.massa, aux->massa, dist);
        seno = abs(player.posy - aux->posy)/dist;
        cosseno = abs(player.posx - aux->posx)/dist;

        direcao = referencial(aux->posx, player.posx);
        forcaxLocal = direcao * forcaGrav * cosseno;
        direcao = referencial(aux->posy, player.posy);
        forcayLocal = direcao * forcaGrav * seno;

        forcax += forcaxLocal;
        forcay += forcayLocal;
        aux = aux->prox;
    }

    //Calculo da força em relação à nave secundária
    dist = distancia(player.posx, player.posy, secundaria.posx, secundaria.posy);
    forcaGrav = forcaGravitacional(player.massa, secundaria.massa, dist);
    seno = abs(secundaria.posy - player.posy)/dist;
    cosseno = abs(secundaria.posx - player.posx)/dist;

    direcao = referencial(secundaria.posx, player.posx);
    forcaxLocal = direcao * forcaGrav * cosseno;
    direcao = referencial(secundaria.posy, player.posy);
    forcayLocal = direcao * forcaGrav * seno;

    forcax += forcaxLocal;
    forcay += forcayLocal;

    //Armazenamento da força resultante
    player.forcax = forcax;
    player.forcay = forcay;
    return player;
}

nave novaPosicaoNave(nave player){
    double posFinal, aceleracaox, aceleracaoy;

    aceleracaox = player.forcax/player.massa;
    aceleracaoy = player.forcay/player.massa;
    
    posFinal = player.posx + (player.velx*MAX_TIME)+ ((aceleracaox*(MAX_TIME)*(MAX_TIME))/2);
    player.posx = posFinal;
    posFinal = player.posy + (player.vely*MAX_TIME)+ ((aceleracaoy*(MAX_TIME)*(MAX_TIME))/2);
    player.posy = posFinal;

    return player;
}

nave novaVelocidadeNave(nave player){
    double velFinal, aceleracaox, aceleracaoy;
    
    aceleracaox = player.forcax/player.massa;
    aceleracaoy = player.forcay/player.massa;

    velFinal = player.velx + (aceleracaox*MAX_TIME);
    player.velx = velFinal;
    velFinal = player.vely + (aceleracaoy*MAX_TIME);
    player.vely = velFinal;

    return player;
}

int escalaNave(double pos, nave *player){
    int novaPosicao;

    novaPosicao = converte(pos, 1);
    if ((*player).posx > T_TOROIDE)
        (*player).posx = -T_TOROIDE;
    if ((*player).posy > T_TOROIDE)
        (*player).posy = -T_TOROIDE;
    if ((*player).posx < -T_TOROIDE)
        (*player).posx = T_TOROIDE;
    if ((*player).posy < -T_TOROIDE)
        (*player).posy = T_TOROIDE;

    return novaPosicao%TAMANHO_TELA;
}

void forcaProjetil(nave player1, nave player2, planeta terra, projetil *tiros){
    int direcao;
    double dist, seno, cosseno;
    double forcaxLocal, forcayLocal, forcax=0, forcay=0, forcaGrav;
    projetil *aux;

    aux = tiros->prox;
    while (aux != NULL){
        forcax=0;
        forcay=0;
        // Calculo da força em relação ao planeta
        dist = distancia(aux->posx, aux->posy, 0, 0);
        forcaGrav = forcaGravitacional(terra.massa, aux->massa, dist);
        seno = abs(aux->posy-0)/dist; 
        cosseno = abs(aux->posx)-0/dist;

        direcao = referencial(0, aux->posx);
        forcaxLocal = direcao * forcaGrav * cosseno;
        direcao = referencial(0, aux->posy);
        forcayLocal = direcao * forcaGrav * seno;
        forcax += forcaxLocal;
        forcay += forcayLocal;

        // Calculo da força em relação à primeira nave
        dist = distancia(aux->posx, aux->posy, player1.posx, player1.posy);
        forcaGrav = forcaGravitacional(aux->massa, player1.massa, dist);
        seno = abs(aux->posy - player1.posy)/dist;
        cosseno = abs(aux->posx - player1.posx)/dist;

        direcao = referencial(player1.posx, aux->posx);
        forcaxLocal = direcao * forcaGrav * cosseno;
        direcao = referencial(player1.posy, aux->posy);
        forcayLocal = direcao * forcaGrav * seno;

        forcax += forcaxLocal;
        forcay += forcayLocal;

        // Calculo da força em relação à segunda nave
        dist = distancia(aux->posx, aux->posy, player2.posx, player2.posy);
        forcaGrav = forcaGravitacional(aux->massa, player2.massa, dist);
        seno = abs(aux->posy - player2.posy)/dist;
        cosseno = abs(aux->posx - player2.posx)/dist;

        direcao = referencial(player2.posx, aux->posx);
        forcaxLocal = direcao * forcaGrav * cosseno;
        direcao = referencial(player2.posy, aux->posy);
        forcayLocal = direcao * forcaGrav * seno;

        forcax += forcaxLocal;
        forcay += forcayLocal;
        aux->forcax = forcax;
        aux->forcay = forcay; 
        aux = aux->prox;
    }
}

void novaPosicaoProjetil(projetil *tiros){
    double posFinal, aceleracaox, aceleracaoy;
    projetil *aux;
    aux = tiros->prox;
    while (aux != NULL){
        aceleracaox = aux->forcax/aux->massa;
        aceleracaoy = aux->forcay/aux->massa;

        posFinal = aux->posx + (aux->velx*MAX_TIME)+ ((aceleracaox*(MAX_TIME)*(MAX_TIME))/2);
        aux->posx = posFinal;
        posFinal = aux->posy + (aux->vely*MAX_TIME)+ ((aceleracaoy*(MAX_TIME)*(MAX_TIME))/2);
        aux->posy = posFinal;
        aux = aux->prox;
    }
}

void novaVelocidadeProjetil(projetil *tiros){
    double velFinal, aceleracaox, aceleracaoy;
    projetil *aux;
    aux = tiros->prox;
    while (aux != NULL){
        aceleracaox = aux->forcax/aux->massa;
        aceleracaoy = aux->forcay/aux->massa;

        velFinal = aux->velx + (aceleracaox*MAX_TIME);
        aux->velx = velFinal;
        velFinal = aux->vely + (aceleracaoy*MAX_TIME);
        aux->vely = velFinal;
        aux = aux->prox;
    }
}

int escalaProjetil(double pos, projetil *tiros){
    int novaPosicao;
    projetil *aux;

    novaPosicao = converte(pos, 1);

    aux = tiros->prox;
    while (aux != NULL){
        if (aux->posx > T_TOROIDE)
            aux->posx = -T_TOROIDE;
        if (aux->posy > T_TOROIDE)
            aux->posy = -T_TOROIDE;
        if (aux->posx < -T_TOROIDE)
            aux->posx = T_TOROIDE;
        if (aux->posy < -T_TOROIDE)
            aux->posy = T_TOROIDE;
        aux = aux->prox;
    }
    return novaPosicao%TAMANHO_TELA;
}



//Controle de jogo
void colisao(nave *player1, nave *player2, projetil *tiros){
    double dist, distMin, posx, posy;
    double raioNave, raioPlaneta, raioTiros;
    projetil *aux, *pai, *aux2;

    raioNave = distancia(converte(0, 2), converte(0, 2), converte(RAIO_NAVE_PX, 2), converte(0, 2));
    raioNave = abs(raioNave);

    raioPlaneta = distancia(converte(0, 2), converte(0, 2), converte(RAIO_PLANETA_PX, 2) , converte(0, 2));
    raioPlaneta = abs(raioPlaneta);

    raioTiros = distancia(converte(0, 2), converte(0, 2), converte(RAIO_PROJETIL_PX, 2) , converte(0, 2));
    raioTiros = abs(raioTiros);

    //Checa se as naves colidiram entre si
    distMin = 2 * raioNave;
    dist = distancia((*player1).posx, (*player1).posy, (*player2).posx, (*player2).posy);
    if (dist <= distMin){
        (*player1).hp = 0;
        (*player2).hp = 0;
        return;
    }
    //Checa se as naves colidiram com os projéteis
    distMin = raioNave + raioTiros;
    pai = tiros;
    aux = tiros->prox;
    while (aux != NULL){
        dist = distancia((*player1).posx, (*player1).posy, aux->posx, aux->posy);
        if (dist <= distMin){
            (*player1).hp -= 100;
            removeProjetil(pai);
            return;
        }
        dist = distancia((*player2).posx, (*player2).posy, aux->posx, aux->posy);
        if (dist <= distMin){
            (*player2).hp -= 100;
            removeProjetil(pai);
            return;
        }
        pai = aux;
        aux = aux->prox;
    }
    //Checa se as naves colidiram com o planeta
    distMin = raioNave + raioPlaneta;
    dist = distancia((*player1).posx, (*player1).posy, 0, 0);
    if (dist <= distMin){
        (*player1).hp = 0;
        return;
    }
    dist = distancia((*player2).posx, (*player2).posy, 0, 0);
    if (dist <= distMin){
        (*player2).hp = 0;
        return;
    }

    //Checa se o tiro colidiu com o planeta
    distMin = raioTiros + raioPlaneta;
    pai = tiros;
    aux = tiros->prox;
    aux2 = NULL;
    while (aux != NULL){
        dist = distancia(aux->posx, aux->posy, 0, 0);
        if (dist <= distMin)
            aux2 = pai;
        pai = aux;
        aux = aux->prox;
        if (aux2 != NULL){
            removeProjetil(aux2);
            aux2 = NULL;
        }
    }
}

nave aceleracaoNave (nave player, int imagemAtual){
    double angulo;
    angulo = 2*M_PI/16;
    angulo = angulo*imagemAtual;
    player.velx += cos(angulo);
    player.vely -= sin(angulo);

    if (player.velx > 30.00)
        player.velx = 30.00;
    else if (player.velx < -30.00)
        player.velx = -30.00;

    if (player.vely > 30.00)
        player.vely = 30.00;
    else if (player.vely < -30.00)
        player.vely = -30.00;
    return player;
}




//Projeteis
projetil *projetilInit(){
    projetil* inicio;
    inicio = malloc(sizeof(projetil));
    inicio->prox = NULL;
    
    return inicio;
}

void projetilFree(projetil* ini){
    projetil *aux;
    while (ini->prox != NULL){
        aux = ini->prox;
        ini->prox = aux->prox;
        free(aux);
    }
    free(ini);
}

void criaProjetil(projetil *tiros, int imagemAtual, double posx, double posy, double cont){
    int i;
    double angulo;
    projetil *novo;

    angulo = 2*M_PI/16;
    angulo = imagemAtual*angulo;
    novo = malloc(sizeof(projetil));
    novo->hp = 100;
    novo->massa = 100;
    novo->duracao = cont+450;
    novo->velx = 50*cos(angulo);
    novo->vely = 50*-sin(angulo);
    novo->forcax = 45*cos(angulo);
    novo->forcay = 45*-sin(angulo);
    novo->posx = posx+(3000*cos(angulo)); //2678+322 = Raio da Nave + fator de erro = 3000.
    novo->posy = posy+(3000*-sin(angulo));
    novo->prox = tiros->prox;
    tiros->prox = novo;
}

void removeProjetil(projetil *anterior){
    projetil *aux;
    aux = anterior->prox;
    anterior->prox = aux->prox;
    free(aux);
}

void duracaoProjetil (projetil *tiros, double cont){
    projetil *aux, *pai, *aux2;
    pai = tiros;
    aux = tiros->prox;
    aux2 = NULL;
    while (aux != NULL){
        if (aux->duracao <= cont)
            aux2 = pai;
        pai = aux;
        aux = aux->prox;
        if (aux2 != NULL){
            removeProjetil(aux2);
            aux2 = NULL;
        }
    }
}