//#################################################################
/*
 Nomes:                                NUSP:
 Erick Rodrigues de Santana          11222008
 Matheus Koiti Ito 	                 9296571        
 Thiago Guerrero                     11275297

------------------------------------------------------------------*/

#include "graphic.h"

//##################################################################

/*Implementação da interface gráfica usada no projeto*/

char nome_nave1[39] = "./Imagens/xpm/Nave1/nave_rotacao01.xpm";
char nome_nave1_mask[40] = "./Imagens/xpm/Nave1/nave_mask01.xpm";
char nome_nave2[50] = "./Imagens/xpm/Nave2/nave2_rotacao01.xpm";
char nome_nave2_mask[50] = "./Imagens/xpm/Nave2/nave2_mask01.xpm";

char nome_score1[34]="./Imagens/xpm/heart0_jogador1.xpm";
char nome_score2[34]="./Imagens/xpm/heart0_jogador2.xpm";
char nome_score_mask[30]="./Imagens/xpm/heart0_mask.xpm";


void aplicaMascara(WINDOW *tela, PIC imagem, MASK mascara, int largura, int altura, int posx, int posy){
    SetMask(tela, mascara);
    PutPic(tela, imagem, 0, 0, largura, altura, posx, posy);
    UnSetMask(tela);
}

void carregaImagemNave (WINDOW *tela, PIC nave[], MASK mask[], PIC aux, int player){
    int i;
    for (i=0; i < 16; i++){
        mask[i] = NewMask(tela, 50, 85);

        //Modifica o nome do arquivo acessado
        if (player == 1){
            nome_nave1[32] = ((i+1) / 10) + 48;
            nome_nave1[33] = ((i+1) % 10) + 48;
            nome_nave1_mask[29] = ((i+1) / 10) + 48;
            nome_nave1_mask[30] = ((i+1) % 10) + 48;
            nave[i] = ReadPic(tela, nome_nave1, NULL);
            aux = ReadPic(tela, nome_nave1_mask, mask[i]);
        }
        else if (player == 2){
            nome_nave2[33]= ((i+1) / 10) + 48;
            nome_nave2[34] = ((i+1) % 10) + 48;
            nome_nave2_mask[30] = ((i+1) / 10) + 48;
            nome_nave2_mask[31] = ((i+1) % 10) + 48;
            nave[i] = ReadPic(tela, nome_nave2, NULL);
            aux = ReadPic(tela, nome_nave2_mask, mask[i]);
        }
    }
}

void carregaScore (WINDOW *tela, PIC scoreP1[], PIC scoreP2[], MASK mask[], PIC aux){
    int i;
    for (i=0; i < 3; i++){
        mask[i] = NewMask(tela, 98, 30);

        nome_score1[19] = i + 48;
        nome_score2[19] = i + 48;
        nome_score_mask[19] = i + 48;
        scoreP1[i] = ReadPic(tela, nome_score1, NULL);
        scoreP2[i] = ReadPic(tela, nome_score2, NULL);
        aux = ReadPic(tela, nome_score_mask, mask[i]);
    }
}

void carregaMenu (WINDOW *tela, PIC Ginicial[], PIC Gfinal[], PIC *Gcomojogar){
    Ginicial[0] = ReadPic(tela, "./Imagens/xpm/menuinicial1.xpm", NULL);
    Ginicial[1] = ReadPic(tela, "./Imagens/xpm/menuinicial2.xpm", NULL);
    Ginicial[2] = ReadPic(tela, "./Imagens/xpm/menuinicial3.xpm", NULL);

    Gfinal[0] = ReadPic(tela, "./Imagens/xpm/jogador1_tente.xpm", NULL);
    Gfinal[1] = ReadPic(tela, "./Imagens/xpm/jogador1_sair.xpm", NULL);
    Gfinal[2] = ReadPic(tela, "./Imagens/xpm/jogador2_tente.xpm", NULL);
    Gfinal[3] = ReadPic(tela, "./Imagens/xpm/jogador2_sair.xpm", NULL);
    Gfinal[4] = ReadPic(tela, "./Imagens/xpm/empatou_tente.xpm", NULL);
    Gfinal[5] = ReadPic(tela, "./Imagens/xpm/empatou_sair.xpm", NULL);
    (*Gcomojogar) = ReadPic(tela, "./Imagens/xpm/comojogar.xpm", NULL);
}

int orientacao(double velx, double vely){
    double normaVel, normaCanonica, prodEscalar;
    double cos, angulo, a;
    int imagem;
    normaCanonica = 1; //(1,0)
    normaVel = norma(velx, vely);
    prodEscalar = velx; //(velx, vey) * (1,0)

    a = acos(prodEscalar/normaVel*normaCanonica);
    if (vely >= 0)
        angulo = a;
    else
        angulo = 2 * M_PI - a;

    return ((int)round((16 * angulo) / (2 * M_PI))) % 16;
    //return 2-1;
}

void ENTER (WINDOW *tela){
    puts("\nDigite ENTER na janela para continuar...");
    while (WGetKey(tela) != 36);
}




//Controle de jogo
int movimentacao (KeyCode tecla, int imagemAtual, int nave){
    if (nave == 1){
        switch (tecla){
        case 114:
            imagemAtual--;
            break;
        case 113:
            imagemAtual++;
            break;
        default:
            break;
        }
    }
    else if (nave == 2){
        switch (tecla){
        case 40:
            imagemAtual--;
            break;
        case 38:
            imagemAtual++;
            break;
        default:
            break;
        }
    }

    if (imagemAtual < 0)
        imagemAtual= 15;
    return (imagemAtual%16);
}