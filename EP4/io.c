//#################################################################
/*
 Nomes:                                NUSP:
 Erick Rodrigues de Santana          11222008
 Matheus Koiti Ito 	                 9296571        
 Thiago Guerrero                     11275297

------------------------------------------------------------------*/

#include "io.h"

//##################################################################

/*Implementação das funções de input e output usadas no projeto*/

int input(planeta *terra, nave *player1, nave *player2){
    FILE * load;
    int vida;
    char in[21]={"./config/default.txt"};

    load = fopen(in, "r");
    if (load == NULL)
        return EXIT_FAILURE;

    fscanf (load, "%lf %d", &((*terra).massa), &vida);
    fscanf (load, "%s %lf %lf %lf %lf %lf", &(*(*player1).nome), &((*player1).massa), &((*player1).posx), &((*player1).posy), &((*player1).velx), &((*player1).vely));
    fscanf (load, "%s %lf %lf %lf %lf %lf", &(*(*player2).nome), &((*player2).massa), &((*player2).posx), &((*player2).posy), &((*player2).velx), &((*player2).vely));
    fclose (load);

    (*player1).hp = vida;
    (*player2).hp = vida;

    return EXIT_SUCCESS;
}

void leituraEntradas (planeta terra, nave player1, nave player2){
    int i;

    printf ("\n################# PRINT DAS ENTRADAS ##################\n");

    printf ("Raio do planeta: %.3lf\n", terra.raio); 
    printf ("Massa do planeta: %.3lf\n", terra.massa); 

    printf ("\nNave 1:\n");
    printf ("Nome: %s\n", player1.nome);
    printf ("Vida: %d\n", player1.hp);
    printf ("Massa: %.3lf\n", player1.massa);
    printf ("Pos x: %.6lf Pos y: %.6lf\n", player1.posx, player1.posy);
    printf ("Vel x: %.6lf Vel y: %.6lf\n", player1.velx, player1.vely);

    printf ("\nNave 2:\n");
    printf ("Nome: %s\n", player2.nome);
    printf ("Vida: %d\n", player2.hp);
    printf ("Massa: %.3lf\n", player2.massa);
    printf ("Pos x: %.10lf Pos y: %.10lf\n", player2.posx, player2.posy);
    printf ("Vel x: %.10lf Vel y: %.10lf\n", player2.velx, player2.vely);
}

void tabelaSaidas(double cont, planeta terra, nave player1, nave player2, projetil *tiros){
    projetil *aux;
    int i;
    
    printf ("\n##############  %4.3lf milésimos após o início ###############\n",cont);
    printf ("Nave 1:\n");
    printf ("Nome: %s\n", player1.nome);
    printf ("Vida: %d\n", player1.hp);
    printf ("Pos x: %.10lf Pos y: %.10lf\n", player1.posx, player1.posy);
    printf ("Vel x: %.10lf Vel y: %.10lf\n", player1.velx, player1.vely);

    printf ("\nNave 2:\n");
    printf ("Nome: %s\n", player2.nome);
    printf ("Vida: %d\n", player2.hp);
    printf ("Pos x: %.10lf Pos y: %.10lf\n", player2.posx, player2.posy);
    printf ("Vel x: %.10lf Vel y: %.10lf\n", player2.velx, player2.vely);

    aux = tiros->prox;
    for (i=1; aux != NULL; i++){ 
        printf ("\nProjétil %d:\n", i);
        printf ("Pos x: %.10lf Pos y: %.10lf\n",aux->posx, aux->posy);
        printf ("Vel x: %.10lf Vel y: %.10lf\n",aux->velx, aux->vely);
    }
}

void atualizaStatus(int *ganhador, int *statusJogo, nave player1, nave player2){
    if (player1.hp == 0 && player2.hp == 0){
    (*ganhador) = 0;
    (*statusJogo) = 0;
    }
    else if (player1.hp == 0){
    (*ganhador) = 2;
    (*statusJogo) = 0;
    }
    else if (player2.hp == 0){
    (*ganhador) = 1;
    (*statusJogo) = 0;
    }
}

int parametros(int argc, char*argv[], int *log){
    if (argc > 1){
        if (!strcmp(argv[1], "-h")){
            printf ("\n      #Parâmetros para execução#\n");
            printf ("\n'-h' - HELP: Mostra todos os parâmetros possíveis;\n");
            printf ("'-l' - LOG: Habilita a exibição do log no terminal;\n");
            printf ("'-lp' - LOG PAUSADO: [DESABILITADO APÓS O EP 4]Habilita a exibição do log no terminal pausando para análise a cada 10 iterações;\n");
            return 2;
        }
        else if(!strcmp(argv[1],"-l")){
            (*log) = 1;
            return EXIT_SUCCESS;
        }
        else{
            printf ("Parâmetro inválido! Use o parâmetro '-h' para ver a lista de parâmetros.\n");
            return EXIT_FAILURE;
        }
    }

    else{
        (*log) = 0;
        return EXIT_SUCCESS;
    }
}

void scoreVida(WINDOW *tela, nave player1, nave player2, PIC imgP1[], PIC imgP2[], MASK mask[]){
    if (player1.hp > 0){
        aplicaMascara(tela, imgP1[(player1.hp/100)-1], mask[(player1.hp/100)-1], 98, 30, 10, 10);
    }
    if (player2.hp > 0){
        aplicaMascara(tela, imgP2[(player2.hp/100)-1], mask[(player2.hp/100)-1], 98, 30, 692, 10);
    }
}

int menuInicial(WINDOW * tela, PIC GMenuInicial[], PIC GComoJogar){
    KeyCode keyAtual;
    int status = 0;
    int contador = 0;

    PutPic(tela, GMenuInicial[0], 0, 0, TAMANHO_TELA, TAMANHO_TELA, 0, 0);
    while (status == 0) {
        if (WCheckKBD(tela)) {
            keyAtual = WGetKey(tela);

            if (keyAtual == 36) {
                if (contador%3 == 1 || contador%3 == -1) {
                    PutPic(tela, GComoJogar, 0, 0, TAMANHO_TELA, TAMANHO_TELA, 0, 0);
                    keyAtual = 0;
                    while (keyAtual != 36)
                        if (WCheckKBD(tela))
                            keyAtual = WGetKey(tela);

                    PutPic(tela, GMenuInicial[1], 0, 0, TAMANHO_TELA, TAMANHO_TELA, 0, 0);
                }
                else if (contador%3 == 2 || contador%3 == -2) {
                    return 0;
                }
                else
                    status = 1;
            }
            else {
                if (keyAtual == 111 || keyAtual == 116) {
                    if (keyAtual == 111 && contador > 0) contador--;
                    else if (keyAtual == 116 && contador < 2) contador++;

                    if (contador%3 == 1 || contador%3 == -1) {
                        PutPic(tela, GMenuInicial[1], 0, 0, TAMANHO_TELA, TAMANHO_TELA, 0, 0);
                    }
                    else if (contador%3 == 2 || contador%3 == -2) {
                        PutPic(tela, GMenuInicial[2], 0, 0, TAMANHO_TELA, TAMANHO_TELA, 0, 0);
                    }
                    else {
                        PutPic(tela, GMenuInicial[0], 0, 0, TAMANHO_TELA, TAMANHO_TELA, 0, 0);
                    }
                }
            }            
        }
    }

    return 1;
}

int fimDeJogo(WINDOW * tela, int ganhador, PIC GFimDeJogo[]){
    KeyCode keyAtual = 0;
    int status = 0;
    int contador = 0;

    if (ganhador == 0){
        PutPic(tela, GFimDeJogo[4], 0, 0, TAMANHO_TELA, TAMANHO_TELA, 0, 0);

        while (keyAtual != 36){
            keyAtual = WGetKey(tela);

            if (keyAtual == 36){
                if (contador%2 == 1 || contador%2 == -1)
                    return 1;
            }
            else if (keyAtual == 111 || keyAtual == 116){
                if (keyAtual == 111) contador--;
                else contador++;

                if (contador%2 == 0){
                    PutPic(tela, GFimDeJogo[4], 0, 0, TAMANHO_TELA, TAMANHO_TELA, 0, 0);
                }
                else{
                    PutPic(tela, GFimDeJogo[5], 0, 0, TAMANHO_TELA, TAMANHO_TELA, 0, 0);
                }
            }
        }
    }

    else if (ganhador == 1){
        PutPic(tela, GFimDeJogo[0], 0, 0, TAMANHO_TELA, TAMANHO_TELA, 0, 0);

        while (keyAtual != 36){
            keyAtual = WGetKey(tela);

            if (keyAtual == 36){
                if (contador%2 == 1 || contador%2 == -1)
                    return 1;
            }
            else if (keyAtual == 111 || keyAtual == 116){
                if (keyAtual == 111) contador--;
                else contador++;

                if (contador%2 == 0){
                    PutPic(tela, GFimDeJogo[0], 0, 0, TAMANHO_TELA, TAMANHO_TELA, 0, 0);
                }
                else{
                    PutPic(tela, GFimDeJogo[1], 0, 0, TAMANHO_TELA, TAMANHO_TELA, 0, 0);
                }
            }
        }
    }

    else{
        PutPic(tela, GFimDeJogo[2], 0, 0, TAMANHO_TELA, TAMANHO_TELA, 0, 0);

        while (keyAtual != 36){
            keyAtual = WGetKey(tela);

            if (keyAtual == 36){
                if (contador%2 == 1 || contador%2 == -1)
                    return 1;
            }
            else if (keyAtual == 111 || keyAtual == 116){
                if (keyAtual == 111) contador--;
                else contador++;

                if (contador%2 == 0){
                    PutPic(tela, GFimDeJogo[2], 0, 0, TAMANHO_TELA, TAMANHO_TELA, 0, 0);
                }
                else{
                    PutPic(tela, GFimDeJogo[3], 0, 0, TAMANHO_TELA, TAMANHO_TELA, 0, 0);
                }
            }
        }
    }
    return 0;
}