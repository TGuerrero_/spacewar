//#################################################################
/*
 Nomes:                                NUSP:
 Erick Rodrigues de Santana          11222008
 Matheus Koiti Ito 	                 9296571        
 Thiago Guerrero                     11275297

------------------------------------------------------------------*/

#ifndef _IO_H
#define _IO_H

#include "fisica.h"
#include "graphic.h"
#include <string.h>

//##################################################################

/*Implementação das funções de input e output usadas no projeto*/

/*
* input();
* Lê todas as entradas de um arquivo.
* Recebe um ponteiro para o tempo total de simulação.
*/
int input(planeta *terra, nave *player1, nave *player2);

/*
* leituraEntradas();
* Recebe todos os corpos e seus status.
* Printa na tela os status da configuração atual.
*/
void leituraEntradas (planeta terra, nave player1, nave player2);

/*
* tabelaSaidas();
* Recebe o valor "cont" do contador atual e o tempo total de simulação.
* Printa na tela os dados dos corpos no momento "cont".
*/
void tabelaSaidas(double cont, planeta terra, nave player1, nave player2, projetil *tiros);

/*
* atualizaStatus();
* Recebe os status das duas naves e, dependendo da vida das naves,
* atualiza o status "ganhador" e "statusJogo" do jogo.
*/
void atualizaStatus(int *ganhador, int *statusJogo, nave player1, nave player2);

/*
* parametros();
* Responsável pelo gerenciamento das opções
* de inicialização.
*/
int parametros(int argc, char*argv[], int *log);

/*
* scoreVida();
* Recebe os status das naves e gerencia a visualização
* da quantidade de vidas restantes.
*/
void scoreVida(WINDOW *tela, nave player1, nave player2, PIC imgP1[], PIC imgP2[], MASK mask[]);

/*
* menuInicial();
* Abre o menu inicial para o jogador;
* retorna 0 se o jogador apertou ENTER no botão sair
* retorna 1 caso o contrario;
*/
int menuInicial(WINDOW * tela, PIC GMenuInicial[], PIC Gcomojogar);

/*
* fimDeJogo();
* Abre o menu de fim de jogo para o jogador;
* retorna 1 se o jogador apertou ENTER no botão sair
* retorna 0 caso o contrario;
*/
int fimDeJogo(WINDOW * tela, int ganhador, PIC GFimDeJogo[]);
#endif