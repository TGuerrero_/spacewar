# SpaceWar
 
 O projeto "SpaceWar" foi proposto pela disciplina "Técnicas de Programação I" lecionada no segundo semestre de 2019 sobre a orientação de Marco Dimas Gubitoso.
 O projeto visa recriar o famoso jogo "SpaceWar", em linguagem C, como uma forma de avaliação da disciplina. Segue descrição do jogo: 
 
"Spacewar! is a space combat video game developed in 1962 by Steve Russell, in collaboration with Martin Graetz and Wayne Wiitanen, and programmed by Russell with assistance from others including Bob Saunders and Steve Piner. It was written for the newly installed DEC PDP-1 at the Massachusetts Institute of Technology. After its initial creation, Spacewar was expanded further by other students and employees of universities in the area, including Dan Edwards and Peter Samson. It was also spread to many of the few dozen, primarily academic, installations of the PDP-1 computer, making Spacewar the first known video game to be played at multiple computer installations.

The game features two spaceships, "the needle" and "the wedge", engaged in a dogfight while maneuvering in the gravity well of a star. Both ships are controlled by human players. Each ship has limited fuel for maneuvering and a limited number of torpedoes, and the ships follow Newtonian physics, remaining in motion even when the player is not accelerating. Flying near the star to provide a gravity assist was a common tactic. Ships are destroyed when hit by a torpedo, colliding with the star, or colliding with each other."
 
 