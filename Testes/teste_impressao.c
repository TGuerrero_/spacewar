#include <stdio.h>	
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include "xwc.h"


char * nomeArquivos[16] = {
    "../Imagens/fighter01-01.xpm",
    "../Imagens/fighter01-02.xpm",
    "../Imagens/fighter01-03.xpm",
    "../Imagens/fighter01-04.xpm",
    "../Imagens/fighter01-05.xpm",
    "../Imagens/fighter01-06.xpm",
    "../Imagens/fighter01-07.xpm",
    "../Imagens/fighter01-08.xpm",
    "../Imagens/fighter01-09.xpm",
    "../Imagens/fighter01-10.xpm",
    "../Imagens/fighter01-11.xpm",
    "../Imagens/fighter01-12.xpm",
    "../Imagens/fighter01-13.xpm",
    "../Imagens/fighter01-14.xpm",
    "../Imagens/fighter01-15.xpm",
    "../Imagens/fighter01-16.xpm"
};

int main() {
    WINDOW *janela;

    // TESTE NAVE
    // PIC nave, auxiliar;
    // MASK mascara_nave;

    // TESTE NAVE COM ANGULAÇÃO
    PIC sprites_nave[16], naves[16], auxiliares[16]; 
    PIC background;
    MASK mascaras[16];
    char nome_arquivo[50] = "../Imagens/fighter01_mask01.xpm"; 
    int i;

    // iniciando janela 
    janela = InitGraph(600, 600, "Testando impressão");
    background = ReadPic(janela, "../Imagens/back.xpm", NULL);

    PutPic(janela, background, 0, 0, 600, 600, 0, 0);
    
    // leitura do teclado 
    InitKBD(janela);
    
    // lendo máscaras da nave
    for (i = 0; i < 16; i++) {
        mascaras[i] = NewMask(janela, 100, 99);
        nome_arquivo[25] = ((i + 1) / 10) + 48;
        nome_arquivo[26] = ((i + 1) % 10) + 48;
        auxiliares[i] = NewPic(janela, 100, 99);
        auxiliares[i] = ReadPic(janela, nome_arquivo, mascaras[i]);
        PutPic(janela, auxiliares[i], 0, 0, 100, 99, 0, 0);
    }

    // lendo naves
    for (i = 0; i < 16; i++)
        sprites_nave[i] = ReadPic(janela, nomeArquivos[i], NULL);

    for (i = 0; i < 16; i++) {
        naves[i] = NewPic(janela, 100, 99);
        PutPic(naves[i], sprites_nave[i], 0, 0, 100, 99, 0, 0);
    }

    UnSetMask(janela);
    for (i = 0;;i++) {
        // TESTE DA ANGULAÇÃO DA NAVE
        SetMask(janela, mascaras[i % 16]);
        PutPic(janela, naves[i % 16], 0, 0, 100, 99, 200, 200);
        usleep(100000);
        WClear(janela);
        UnSetMask(janela);
        PutPic(janela, background, 0, 0, 600, 600, 0, 0);
    }

    return (0);
}